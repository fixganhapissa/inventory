/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.26 : Database - techno_inventory
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`techno_inventory` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `techno_inventory`;

/*Table structure for table `alat` */

DROP TABLE IF EXISTS `alat`;

CREATE TABLE `alat` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `no_seri` varchar(100) DEFAULT NULL,
  `tanggal_kalibrasi` date DEFAULT NULL,
  `program_kalibrasi_ulang` date DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `kategori` int(1) DEFAULT NULL COMMENT '1:alat kerja, 2:alat pantau, 3:alat ukur',
  `keterangan` varchar(255) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_alat_ukur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `alat` */

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL COMMENT '1:active, 2:disabled',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang` */

/*Table structure for table `barang_keluar` */

DROP TABLE IF EXISTS `barang_keluar`;

CREATE TABLE `barang_keluar` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `inv_no` varchar(15) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `proyek` varchar(255) DEFAULT NULL,
  `tanggal_permintaan` date DEFAULT NULL,
  `tanggal_pengeluaran` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0:Menunggu Persetujuan, 1:Disetujui, 2:Ditolak',
  `id_user` int(16) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang_keluar` */

/*Table structure for table `barang_masuk` */

DROP TABLE IF EXISTS `barang_masuk`;

CREATE TABLE `barang_masuk` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `inv_no` varchar(15) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `proyek` varchar(255) DEFAULT NULL,
  `tanggal_penyerahan` date DEFAULT NULL,
  `id_user` int(16) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang_masuk` */

/*Table structure for table `detail_barang_keluar` */

DROP TABLE IF EXISTS `detail_barang_keluar`;

CREATE TABLE `detail_barang_keluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang_keluar` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_barang_keluar` */

/*Table structure for table `detail_barang_masuk` */

DROP TABLE IF EXISTS `detail_barang_masuk`;

CREATE TABLE `detail_barang_masuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang_masuk` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_barang_masuk` */

/*Table structure for table `detail_peminjaman` */

DROP TABLE IF EXISTS `detail_peminjaman`;

CREATE TABLE `detail_peminjaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) DEFAULT NULL,
  `id_alat` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_peminjaman` */

/*Table structure for table `detail_pengembalian` */

DROP TABLE IF EXISTS `detail_pengembalian`;

CREATE TABLE `detail_pengembalian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengembalian` int(11) DEFAULT NULL,
  `id_material` int(11) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_pengembalian` */

/*Table structure for table `detail_permintaan` */

DROP TABLE IF EXISTS `detail_permintaan`;

CREATE TABLE `detail_permintaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_permintaan` int(11) DEFAULT NULL,
  `id_material` int(11) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_permintaan` */

/*Table structure for table `material` */

DROP TABLE IF EXISTS `material`;

CREATE TABLE `material` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `spesifikasi` varchar(255) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `kondisi` varchar(100) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL COMMENT '1:active, 2:disabled',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `material` */

/*Table structure for table `mesin` */

DROP TABLE IF EXISTS `mesin`;

CREATE TABLE `mesin` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `spesifikasi` varchar(255) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `kondisi` varchar(100) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_mesin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mesin` */

/*Table structure for table `peminjaman` */

DROP TABLE IF EXISTS `peminjaman`;

CREATE TABLE `peminjaman` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `proyek` varchar(255) DEFAULT NULL,
  `tanggal_peminjaman` date DEFAULT NULL,
  `tanggal_pengembalian` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1:dipinjam, 2:dikembalikan',
  `id_user` int(16) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `peminjaman` */

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `proyek` varchar(255) DEFAULT NULL,
  `tanggal_pengembalian` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0:Menunggu Persetujuan, 1:Disetujui, 2:Ditolak',
  `id_user` int(16) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pengembalian` */

/*Table structure for table `permintaan` */

DROP TABLE IF EXISTS `permintaan`;

CREATE TABLE `permintaan` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `proyek` varchar(255) DEFAULT NULL,
  `tanggal_permintaan` date DEFAULT NULL,
  `tanggal_penyerahan` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0:Menunggu Persetujuan, 1:Disetujui, 2:Ditolak',
  `id_user` int(16) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `permintaan` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(16) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `jenis_kelamin` enum('Laki - laki','Perempuan') DEFAULT NULL,
  `agama` enum('Islam','Kristen','Hindu','Budha') DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telp` varchar(16) DEFAULT NULL,
  `no_npwp` varchar(16) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `level` int(1) NOT NULL DEFAULT '1' COMMENT '1 = Admin, 2 = Logistik, 3 = Pegawai, 4 = Manajemen',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nik`,`nama_lengkap`,`email`,`username`,`password`,`jenis_kelamin`,`agama`,`alamat`,`no_telp`,`no_npwp`,`photo`,`kategori`,`level`) values (1,NULL,'Admin','admin@gmail.com','admin','21232f297a57a5a743894a0e4a801fc3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(2,NULL,'Logistik','logistik@gmail.com','logistik','cb1f02561c07f62717a4814c048a6239',NULL,NULL,NULL,'08745456456',NULL,NULL,NULL,2),(3,NULL,'Pegawai','pegawai@gmail.com','pegawai','047aeeb234644b9e2d4138ed3bc7976a',NULL,NULL,NULL,'085722766117',NULL,NULL,NULL,3),(4,NULL,'Manajemen','manajemen@gmail.com','manajemen','19b51f1cbb6146adcacbce46d5bdc3f2',NULL,NULL,NULL,'085759130919',NULL,NULL,NULL,4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
