<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_permintaan extends CI_Model {

	public function get($id = null) 
	{
		$this->db->from('permintaan');
		if($id != null){
			$this->db->where('id', $id); //parameter where
		}
		$query = $this->db->get();
		return $query;
	}

    public function getPermintaanData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM permintaan WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM permintaan ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    // get detail_permintaan
    public function getDetailPermintaanData($id_permintaan = null)
    {
        if(!$id_permintaan) {
            return false;
        }

        $sql = "SELECT * FROM detail_permintaan WHERE id_permintaan = ?";
        $query = $this->db->query($sql, array($id_permintaan));
        return $query->result_array();
    }
	
	public function create()
	{
        $day = date("dmy");
		$id_user = $this->session->userdata('id_user');
		$inv_no = 'INV-'.$day.'-'.strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, 4));
    	$data = array(
    		'nama' => $this->input->post('nama'),
    		'no_telp' => $this->input->post('no_telp'),
            'proyek' => $this->input->post('proyek'),
            'tanggal_permintaan' => $this->input->post('tanggal_permintaan'),
    		'tanggal_penyerahan' => $this->input->post('tanggal_penyerahan'),
            'status' => $this->input->post('status'),
    		'id_user' => $id_user
    	);

		$insert = $this->db->insert('permintaan', $data);
		$id_permintaan = $this->db->insert_id();

		$this->load->model('M_material');

		$count_material = count($this->input->post('material'));
    	for($x = 0; $x < $count_material; $x++) {
    		$items = array(
    			'id_permintaan' => $id_permintaan,
    			'id_material' => $this->input->post('material')[$x],
                'type' => $this->input->post('type')[$x],
                'vendor' => $this->input->post('vendor')[$x],
                'qty' => $this->input->post('qty')[$x],
    			'keterangan' => $this->input->post('keterangan')[$x],
    		);

    		$this->db->insert('detail_permintaan', $items);

    		// kurang stock dari material
    		$material_data = $this->M_material->getMaterialData($this->input->post('material')[$x]);
    		$qty = (int) $material_data['qty'] + (int) $this->input->post('qty')[$x];

    		$update_material = array('qty' => $qty);
    		$this->M_material->update($update_material, $this->input->post('material')[$x]);
    	}

		return ($id_permintaan) ? $id_permintaan : false;
	}
	

    public function update($id)
    {
        // if($id) {
            $id_user = $this->session->userdata('id_user');
            // fetch data permintaan 

            $data = array(
                'nama' => $this->input->post('nama'),
                'no_telp' => $this->input->post('no_telp'),
                'proyek' => $this->input->post('proyek'),
                'tanggal_permintaan' => $this->input->post('tanggal_permintaan'),
                'tanggal_penyerahan' => $this->input->post('tanggal_penyerahan'),
                'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s'),
                'id_user' => $id_user
            );

            $this->db->where('id', $id);
            $update = $this->db->update('permintaan', $data);

            // detail permintaan
            // pertama akan mengganti qty material dan subtract qty lagi
            $this->load->model('M_material');
            $get_detail_permintaan = $this->getDetailPermintaanData($id);
            foreach ($get_detail_permintaan as $k => $v) {
                $id_material = $v['id_material'];
                $qty = $v['qty'];
                // get material
                $data_material = $this->M_material->getMaterialData($id_material);
                $update_qty = $data_material['qty'] - $qty;
                $update_data_material = array('qty' => $update_qty);
                
                // update qty material
                $this->M_material->update($update_data_material, $id_material);
            }

            // remove material di detail permintaan 
            $this->db->where('id_permintaan', $id);
            $this->db->delete('detail_permintaan');

            // tambah qty material
            $count_material= count($this->input->post('material'));
            for($x = 0; $x < $count_material; $x++) {
                $items = array(
                    'id_permintaan' => $id,
                    'id_material' => $this->input->post('material')[$x],
                    'qty' => $this->input->post('qty')[$x],
                    'type' => $this->input->post('type')[$x],
                    'vendor' => $this->input->post('vendor')[$x],
                    'keterangan' => $this->input->post('keterangan')[$x],
                );
                $this->db->insert('detail_permintaan', $items);

                // tambah stock material
                $data_material = $this->M_material->getMaterialData($this->input->post('material')[$x]);
                $qty = (int) $data_material['qty'] + (int) $this->input->post('qty')[$x];

                $update_material = array('qty' => $qty);
                $this->M_material->update($update_material, $this->input->post('material')[$x]);
            }

            return true;
        // }
    }

    public function remove($id)
    {
        if($id) {
            $this->db->where('id', $id);
            $delete = $this->db->delete('permintaan');

            $this->db->where('id_permintaan', $id);
            $delete_item = $this->db->delete('detail_permintaan');
            return ($delete == true && $delete_item) ? true : false;
        }
    }

    

}