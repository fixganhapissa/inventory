<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang_masuk extends CI_Model {

	public function get($id = null) 
	{
		$this->db->from('barang_masuk');
		if($id != null){
			$this->db->where('id', $id); //parameter where

		}
		$query = $this->db->get();
		return $query;
	}

    public function getBarangMasukData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM barang_masuk WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM barang_masuk ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    // get detail_brang_masuk data
    public function getDetailBarangMasukData($id_barang_masuk = null)
    {
        if(!$id_barang_masuk) {
            return false;
        }
        $sql = "SELECT detail_barang_masuk.*, barang.`nama` as nama_barang FROM detail_barang_masuk, barang WHERE id_barang_masuk = ? AND barang.`id` = detail_barang_masuk.`id_barang`  ";
        $query = $this->db->query($sql, array($id_barang_masuk));
        return $query->result_array();
    }
	
	public function create()
	{
        $day = date("dmy");
		$id_user = $this->session->userdata('id_user');
		$inv_no = 'INV-'.$day.'-'.strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, 4));
    	$data = array(
    		'inv_no' => $inv_no,
    		'nama' => $this->input->post('nama'),
    		'no_telp' => $this->input->post('no_telp'),
    		'proyek' => $this->input->post('proyek'),
    		'tanggal_penyerahan' => date('Y-m-d'),
    		'id_user' => $id_user
    	);

		$insert = $this->db->insert('barang_masuk', $data);
		$id_barang_masuk = $this->db->insert_id();

		$this->load->model('M_barang');
		$count_barang = count($this->input->post('barang'));
    	for($x = 0; $x < $count_barang; $x++) {
    		$items = array(
    			'id_barang_masuk' => $id_barang_masuk,
    			'id_barang' => $this->input->post('barang')[$x],
                'type' => $this->input->post('type_value')[$x],
                'vendor' => $this->input->post('vendor_value')[$x],
                'qty' => $this->input->post('qty')[$x],
    			'keterangan' => $this->input->post('keterangan')[$x],
    		);

    		$this->db->insert('detail_barang_masuk', $items);

    		// tambah stock dari barang
    		$barang_data = $this->M_barang->getBarangData($this->input->post('barang')[$x]);
    		$qty = (int) $barang_data['qty'] + (int) $this->input->post('qty')[$x];

    		$update_barang = array('qty' => $qty);


    		$this->M_barang->update($update_barang, $this->input->post('barang')[$x]);
    	}

		return ($id_barang_masuk) ? $id_barang_masuk : false;
	}
	

    public function update($id)
    {
        // if($id) {
            $id_user = $this->session->userdata('id_user');
            // fetch data barang masuk 

            $data = array(
                'nama' => $this->input->post('nama'),
                'no_telp' => $this->input->post('no_telp'),
                'proyek' => $this->input->post('proyek'),
                'tanggal_penyerahan' => date('Y-m-d'),
                'updated_at' => date('Y-m-d H:i:s'),
                'id_user' => $id_user
            );

            $this->db->where('id', $id);
            $update = $this->db->update('barang_masuk', $data);

            // detail barang masuk
            // pertama akan mengganti qty barang dan subtract qty lagi
            $this->load->model('M_barang');
            $get_detail_barang_masuk = $this->getDetailBarangMasukData($id);
            foreach ($get_detail_barang_masuk as $k => $v) {
                $id_barang = $v['id_barang'];
                $qty = $v['qty'];
                // get barang
                $data_barang = $this->M_barang->getBarangData($id_barang);
                $update_qty = $data_barang['qty'] - $qty;
                $update_data_barang = array('qty' => $update_qty);
                
                // update qty barang
                $this->M_barang->update($update_data_barang, $id_barang);
            }

            // remove barang di detail barang masuk data 
            $this->db->where('id_barang_masuk', $id);
            $this->db->delete('detail_barang_masuk');

            // tambah qty barang
            $count_barang = count($this->input->post('barang'));
            for($x = 0; $x < $count_barang; $x++) {
                $items = array(
                    'id_barang_masuk' => $id,
                    'id_barang' => $this->input->post('barang')[$x],
                    'qty' => $this->input->post('qty')[$x],
                    'type' => $this->input->post('type_value')[$x],
                    'vendor' => $this->input->post('vendor_value')[$x],
                    'keterangan' => $this->input->post('keterangan')[$x],
                );
                $this->db->insert('detail_barang_masuk', $items);

                // tambah stock barang
                $data_barang = $this->M_barang->getBarangData($this->input->post('barang')[$x]);
                $qty = (int) $data_barang['qty'] + (int) $this->input->post('qty')[$x];

                $update_barang = array('qty' => $qty);
                $this->M_barang->update($update_barang, $this->input->post('barang')[$x]);
            }

            return true;
        // }
    }

    public function remove($id)
    {
        if($id) {
            $this->db->where('id', $id);
            $delete = $this->db->delete('barang_masuk');

            $this->db->where('id_barang_masuk', $id);
            $delete_item = $this->db->delete('detail_barang_masuk');
            return ($delete == true && $delete_item) ? true : false;
        }
    }

    

}