<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_peminjaman extends CI_Model {

	public function get($id = null) 
	{
		$this->db->from('peminjaman');
		if($id != null){
			$this->db->where('id', $id); //parameter where

		}
		$query = $this->db->get();
		return $query;
	}

    public function getPeminjamanData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM peminjaman WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM peminjaman ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    // get detail_peminjaman data
    public function getDetailPeminjamanData($id_peminjaman = null)
    {
        if(!$id_peminjaman) {
            return false;
        }

        $sql = "SELECT detail_peminjaman.*, alat.`nama` as nama_alat FROM detail_peminjaman, alat WHERE id_peminjaman = ? AND alat.`id` = detail_peminjaman.`id_alat`  ";
        $query = $this->db->query($sql, array($id_peminjaman));
        return $query->result_array();
    }
	
	public function create()
	{
        $day = date("dmy");
		$id_user = $this->session->userdata('id_user');
		// $inv_no = 'INV-'.$day.'-'.strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, 4));
    	$data = array(
    		// 'inv_no' => $inv_no,
    		'nama' => $this->input->post('nama'),
    		'no_telp' => $this->input->post('no_telp'),
            'proyek' => $this->input->post('proyek'),
            'tanggal_peminjaman' => $this->input->post('tanggal_peminjaman'),
    		'tanggal_pengembalian' => $this->input->post('tanggal_pengembalian'),
            'status' => 1,
    		'id_user' => $id_user
    	);

		$insert = $this->db->insert('peminjaman', $data);
		$id_peminjaman = $this->db->insert_id();

		$this->load->model('M_alat');

		$count_alat = count($this->input->post('alat'));
    	for($x = 0; $x < $count_alat; $x++) {
    		$items = array(
    			'id_peminjaman' => $id_peminjaman,
    			'id_alat' => $this->input->post('alat')[$x],
                'qty' => $this->input->post('qty')[$x],
                'status' => $this->input->post('status')[$x],
    			'keterangan' => $this->input->post('keterangan')[$x],
    		);

    		$this->db->insert('detail_peminjaman', $items);

    		// kurang stock dari alat
    		$alat_data = $this->M_alat->getAlatData($this->input->post('alat')[$x]);
    		$qty = (int) $alat_data['qty'] - (int) $this->input->post('qty')[$x];

    		$update_alat = array('qty' => $qty);


    		$this->M_alat->update($update_alat, $this->input->post('alat')[$x]);
    	}

		return ($id_peminjaman) ? $id_peminjaman : false;
	}
	

    public function update($id)
    {
        // if($id) {
            $id_user = $this->session->userdata('id_user');
            // fetch data peminjaman

            $data = array(
                'nama' => $this->input->post('nama'),
                'no_telp' => $this->input->post('no_telp'),
                'proyek' => $this->input->post('proyek'),
                'tanggal_peminjaman' => $this->input->post('tanggal_peminjaman'),
                'tanggal_pengembalian' => $this->input->post('tanggal_pengembalian'),
                // 'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s'),
                'id_user' => $id_user
            );

            $this->db->where('id', $id);
            $update = $this->db->update('peminjaman', $data);

            // detail peminjaman
            // pertama akan mengganti qty alat dan subtract qty lagi
            $this->load->model('M_peminjaman');
            $get_detail_peminjaman = $this->getDetailPeminjamanData($id);
            foreach ($get_detail_peminjaman as $k => $v) {
                $id_alat = $v['id_alat'];
                $qty = $v['qty'];
                // get alat
                $data_alat = $this->M_alat->getAlatData($id_alat);
                $update_qty = $data_alat['qty'] + $qty;
                $update_data_alat = array('qty' => $update_qty);
                
                // update qty alat
                $this->M_alat->update($update_data_alat, $id_alat);
            }

            // remove alat di detail peminjaman keluar data 
            $this->db->where('id_peminjaman', $id);
            $this->db->delete('detail_peminjaman');

            // tambah qty alat
            $count_alat = count($this->input->post('alat'));
            for($x = 0; $x < $count_alat; $x++) {
                $items = array(
                    'id_peminjaman' => $id,
                    'id_alat' => $this->input->post('alat')[$x],
                    'qty' => $this->input->post('qty')[$x],
                    'status' => $this->input->post('status')[$x],
                    'keterangan' => $this->input->post('keterangan')[$x],
                );
                $this->db->insert('detail_peminjaman', $items);

                // tambah stock alat
                $data_alat = $this->M_alat->getAlatData($this->input->post('alat')[$x]);
                $qty = (int) $data_alat['qty'] - (int) $this->input->post('qty')[$x];

                $update_alat = array('qty' => $qty);
                $this->M_alat->update($update_alat, $this->input->post('alat')[$x]);
            }

            return true;
        // }
    }

    public function pengembalian($id)
    {   
        $id_user = $this->session->userdata('id_user');
        // update data peminjaman
        $data = array(
            'status' => $this->input->post('status'),
            'updated_at' => date('Y-m-d H:i:s'),
            'id_user' => $id_user
        );
        $this->db->where('id', $id);
        $update = $this->db->update('peminjaman', $data);

        // detail peminjaman
        // pertama akan mengganti qty alat dan subtract qty lagi
        $this->load->model('M_peminjaman');
        $get_detail_peminjaman = $this->getDetailPeminjamanData($id);
        foreach ($get_detail_peminjaman as $k => $v) {
            $id_alat = $v['id_alat'];
            $qty = $v['qty'];
            // get alat
            $data_alat = $this->M_alat->getAlatData($id_alat);
            $update_qty = $data_alat['qty'] + $qty;
            $update_data_alat = array('qty' => $update_qty);
            // update qty alat
            $this->M_alat->update($update_data_alat, $id_alat);
        }

        return true;
    }

    public function remove($id)
    {
        if($id) {
            $this->db->where('id', $id);
            $delete = $this->db->delete('peminjaman');

            $this->db->where('id_peminjaman', $id);
            $delete_item = $this->db->delete('detail_peminjaman');
            return ($delete == true && $delete_item) ? true : false;
        }
    }

    

}