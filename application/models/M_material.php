<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_material extends CI_Model {
	
	// fungsi untuk menampilkan semua data
	public function get($id = null) 
	{
		$this->db->from('material');
		if($id != null){
			$this->db->where('id', $id); //menampilkan data sesuai parameter where

		}
		$query = $this->db->get();
		return $query;
	}

	public function getMaterialData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM material where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM material ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getActiveMaterialData()
	{
		$sql = "SELECT * FROM material WHERE is_active = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('material', $data);
			return ($update == true) ? true : false;
		}
	}

	public function add($post)
	{
		$params = [
			// 'id'			=> $post['id'],
			'nama'			=> $post['nama'],
			'model' 	 	=> $post['model'],
			'kondisi' 	 	=> $post['kondisi'],
			'lokasi'	 	=> $post['lokasi'],
			'qty'	 		=> $post['qty'],
			'is_active'		=> 1,
			'spesifikasi' 	=> empty($post['spesifikasi']) ? null : $post['spesifikasi'],
		];
		$this->db->insert('material',$params);

	}
	public function edit($post)
	{
		$params = [
			'nama'			=> $post['nama'],
			'model' 	 	=> $post['model'],
			'kondisi' 	 	=> $post['kondisi'],
			'lokasi'	 	=> $post['lokasi'],
			'qty'	 		=> $post['qty'],
			'spesifikasi' 	=> empty($post['spesifikasi']) ? null : $post['spesifikasi'],
			'updated_at'	=> date('Y-m-d H:i:s')
		];
		$this->db->where('id', $post['id']);
		$this->db->update('material',$params);

	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('material');
	}


}
