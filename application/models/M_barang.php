<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {
	
	// fungsi untuk menampilkan semua data 
	public function get($id = null) 
	{
		$this->db->from('barang');
		if($id != null){
			$this->db->where('id', $id); //menampilkan data sesuai parameter where

		}
		$query = $this->db->get();
		return $query;
	}

	public function getBarangData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM barang where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM barang ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getActiveBarangData()
	{
		$sql = "SELECT * FROM barang WHERE is_active = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('barang', $data);
			return ($update == true) ? true : false;
		}
	}


	public function add($post)
	{
		$params = [
			'id'			=> $post['id'],
			'nama'			=> $post['nama'],
			'type' 	 		=> $post['type'],
			'vendor' 	 	=> $post['vendor'],
			'qty'	 		=> $post['qty'],
			'is_active'		=> 1,
			'deskripsi' 	=> empty($post['deskripsi']) ? null : $post['deskripsi'],
		];
		$this->db->insert('barang',$params);

	}
	public function edit($post)
	{
		$params = [
			'nama'			=> $post['nama'],
			'nama'			=> $post['nama'],
			'type' 	 		=> $post['type'],
			'vendor' 	 	=> $post['vendor'],
			'qty'	 		=> $post['qty'],
			'deskripsi' 	=> empty($post['deskripsi']) ? null : $post['deskripsi'],
			'updated_at'	=> date('Y-m-d H:i:s')
		];
		$this->db->where('id', $post['id']);
		$this->db->update('barang',$params);

	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('barang');
	}


}
