<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class M_laporan_barang_masuk extends CI_Model {
	
	public function cetak_detail_barang($id) {
        $hasil = $this->db->query("SELECT detail_barang_masuk.*, barang.`nama` as nama_barang FROM detail_barang_masuk, barang WHERE id_barang_masuk = $id AND barang.`id` = detail_barang_masuk.`id_barang` ");
        return $hasil->result();
    }
    
    public function cetak_barang_masuk($id) {
        $hasil = $this->db->query("SELECT barang_masuk.* FROM barang_masuk WHERE id = $id ");
        return $hasil->result();
    }

    public function filter($table, $start_date, $end_date){
        $this->db->where('start_date >=',$start_date); 
        $this->db->where('end_date <=',$end_date);
        return $this->db->get($table)->result();
    }

}
