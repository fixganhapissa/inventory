<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {
	
	public function login($post)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $post['username']);
		$this->db->where('password', md5($post['password']));
		$query = $this->db->get();
		return $query;
	}

	// fungsi untuk menampilkan data setiap user 
	public function get($id_user = null) 
	{
		$this->db->from('user');
		if($id_user != null){
			$this->db->where('id_user', $id_user); //menampilkan user sesuai parameter where

		}
		$query = $this->db->get();
		return $query;
	}


}
