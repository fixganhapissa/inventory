<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengembalian extends CI_Model {

	public function get($id = null) 
	{
		$this->db->from('pengembalian');
		if($id != null){
			$this->db->where('id', $id); //parameter where
		}
		$query = $this->db->get();
		return $query;
	}

    public function getPengembalianData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM pengembalian WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM pengembalian ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    // get detail_pengembalian
    public function getDetailPengembalianData($id_pengembalian = null)
    {
        if(!$id_pengembalian) {
            return false;
        }

        $sql = "SELECT * FROM detail_pengembalian WHERE id_pengembalian = ?";
        $query = $this->db->query($sql, array($id_pengembalian));
        return $query->result_array();
    }
	
	public function create()
	{
        $day = date("dmy");
		$id_user = $this->session->userdata('id_user');
		$inv_no = 'INV-'.$day.'-'.strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, 4));
    	$data = array(
    		'nama' => $this->input->post('nama'),
    		'no_telp' => $this->input->post('no_telp'),
            'proyek' => $this->input->post('proyek'),
            'tanggal_pengembalian' => $this->input->post('tanggal_pengembalian'),
            'status' => $this->input->post('status'),
    		'id_user' => $id_user
    	);

		$insert = $this->db->insert('pengembalian', $data);
		$id_pengembalian = $this->db->insert_id();

		$this->load->model('M_material');

		$count_material = count($this->input->post('material'));
    	for($x = 0; $x < $count_material; $x++) {
    		$items = array(
    			'id_pengembalian' => $id_pengembalian,
    			'id_material' => $this->input->post('material')[$x],
                'type' => $this->input->post('type')[$x],
                'vendor' => $this->input->post('vendor')[$x],
                'qty' => $this->input->post('qty')[$x],
    			'keterangan' => $this->input->post('keterangan')[$x],
    		);

    		$this->db->insert('detail_pengembalian', $items);

    		// tambah stock dari material
    		$material_data = $this->M_material->getMaterialData($this->input->post('material')[$x]);
    		$qty = (int) $material_data['qty'] + (int) $this->input->post('qty')[$x];

    		$update_material = array('qty' => $qty);
    		$this->M_material->update($update_material, $this->input->post('material')[$x]);
    	}

		return ($id_pengembalian) ? $id_pengembalian : false;
	}
	

    public function update($id)
    {
        // if($id) {
            $id_user = $this->session->userdata('id_user');
            // fetch data pengembalian 

            $data = array(
                'nama' => $this->input->post('nama'),
                'no_telp' => $this->input->post('no_telp'),
                'proyek' => $this->input->post('proyek'),
                'tanggal_pengembalian' => $this->input->post('tanggal_pengembalian'),
                'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s'),
                'id_user' => $id_user
            );

            $this->db->where('id', $id);
            $update = $this->db->update('pengembalian', $data);

            // detail pengembalian
            // pertama akan mengganti qty material dan subtract qty lagi
            $this->load->model('M_material');
            $get_detail_pengembalian = $this->getDetailPengembalianData($id);
            foreach ($get_detail_pengembalian as $k => $v) {
                $id_material = $v['id_material'];
                $qty = $v['qty'];
                // get material
                $data_material = $this->M_material->getMaterialData($id_material);
                $update_qty = $data_material['qty'] - $qty;
                $update_data_material = array('qty' => $update_qty);
                
                // update qty material
                $this->M_material->update($update_data_material, $id_material);
            }

            // remove material di detail pengembalian 
            $this->db->where('id_pengembalian', $id);
            $this->db->delete('detail_pengembalian');

            // tambah qty material
            $count_material= count($this->input->post('material'));
            for($x = 0; $x < $count_material; $x++) {
                $items = array(
                    'id_pengembalian' => $id,
                    'id_material' => $this->input->post('material')[$x],
                    'qty' => $this->input->post('qty')[$x],
                    'type' => $this->input->post('type')[$x],
                    'vendor' => $this->input->post('vendor')[$x],
                    'keterangan' => $this->input->post('keterangan')[$x],
                );
                $this->db->insert('detail_pengembalian', $items);

                // tambah stock material
                $data_material = $this->M_material->getMaterialData($this->input->post('material')[$x]);
                $qty = (int) $data_material['qty'] - (int) $this->input->post('qty')[$x];

                $update_material = array('qty' => $qty);
                $this->M_material->update($update_material, $this->input->post('material')[$x]);
            }

            return true;
        // }
    }

    public function remove($id)
    {
        if($id) {
            $this->db->where('id', $id);
            $delete = $this->db->delete('pengembalian');

            $this->db->where('id_pengembalian', $id);
            $delete_item = $this->db->delete('detail_pengembalian');
            return ($delete == true && $delete_item) ? true : false;
        }
    }

    

}