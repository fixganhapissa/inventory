<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_stok_barang extends CI_Model {

	public function get($id = null) 
	{
		$this->db->from('barang_keluar');
		if($id != null){
			$this->db->where('id', $id); //parameter where

		}
		$query = $this->db->get();
		return $query;
	}

    public function getBarangData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM barang WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM barang ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function getBarangKeluarData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM barang_keluar WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM barang_keluar ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    // get detail_brang_keluar data
    public function getDetailBarangKeluarData($id_barang_keluar = null)
    {
        if(!$id_barang_keluar) {
            return false;
        }

        $sql = "SELECT detail_barang_keluar.*, barang.`nama` as nama_barang, barang.`deskripsi` as spesifikasi, barang.`qty` as sisa, barang_keluar.`tanggal_pengeluaran` 
                FROM detail_barang_keluar, barang, barang_keluar 
                WHERE id_barang = ? AND barang.`id` = detail_barang_keluar.`id_barang` AND barang_keluar.id = detail_barang_keluar.id_barang_keluar  ";
        $query = $this->db->query($sql, array($id_barang_keluar));
        return $query->result_array();
    }   

}