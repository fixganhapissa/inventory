<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_alat_pantau extends CI_Model {
	
	// fungsi untuk menampilkan semua data
	public function get($id = null) 
	{
		$this->db->from('alat');
		$this->db->where('kategori', 2);
		if($id != null){

			// $this->db->where($array);
			// $this->db->where('id', $id); //parameter where
			$array = array('id' => $id, 'kategori' => 2);
			$this->db->where($array); //parameter where
		}
		$query = $this->db->get();
		return $query;
	}

	public function add($post)
	{
		$params = [
			'id'				=> $post['id'],
			'nama'				=> $post['nama'],
			'no_seri' 	 		=> $post['no_seri'],
			'tanggal_kalibrasi' => $post['tanggal_kalibrasi'],
			'program_kalibrasi_ulang' => $post['program_kalibrasi_ulang'],
			'qty' 	 			=> $post['qty'],
			'is_active'			=> 1,
			'kategori' 	 		=> 2,
			'keterangan' 		=> empty($post['keterangan']) ? null : $post['keterangan'],
		];
		$this->db->insert('alat',$params);

	}
	public function edit($post)
	{
		$params = [
			'nama'				=> $post['nama'],
			'no_seri' 	 		=> $post['no_seri'],
			'tanggal_kalibrasi' => $post['tanggal_kalibrasi'],
			'program_kalibrasi_ulang' => $post['program_kalibrasi_ulang'],
			'qty' 				=> $post['qty'],
			'keterangan' 		=> empty($post['keterangan']) ? null : $post['keterangan'],
			'updated_at'		=> date('Y-m-d H:i:s')
		];
		$this->db->where('id', $post['id']);
		$this->db->update('alat',$params);

	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('alat');
	}


}
