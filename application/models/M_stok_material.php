<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_stok_material extends CI_Model {

    public function get($id = null) 
    {
        $this->db->from('permintaan');
        if($id != null){
            $this->db->where('id', $id); //parameter where

        }
        $query = $this->db->get();
        return $query;
    }

    public function getMaterialData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM material WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM material ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function getPermintaanData($id = null)
    {
        if($id) {
            $sql = "SELECT * FROM permintaan WHERE id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM permintaan ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    // get detail_brang_keluar data
    public function getDetailPermintaanData($id_permintaan = null)
    {
        if(!$id_permintaan) {
            return false;
        }

        $sql = "SELECT detail_permintaan.*, material.`nama` AS nama_material, permintaan.`tanggal_permintaan` 
                FROM detail_permintaan, material, permintaan 
                WHERE id_material = ? AND material.`id` = detail_permintaan.`id_material` 
                AND permintaan.id = detail_permintaan.id_permintaan";
        $query = $this->db->query($sql, array($id_permintaan));
        return $query->result_array();
    }   

}