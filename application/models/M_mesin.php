<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mesin extends CI_Model {
	
	// fungsi untuk menampilkan semua data 
	public function get($id = null) 
	{
		$this->db->from('mesin');
		if($id != null){
			$this->db->where('id', $id); //menampilkan data sesuai parameter where

		}
		$query = $this->db->get();
		return $query;
	}

	public function add($post)
	{
		$params = [
			// 'id'			=> $post['id'],
			'nama'			=> $post['nama'],
			'model' 	 	=> $post['model'],
			'kondisi' 	 	=> $post['kondisi'],
			'lokasi'	 	=> $post['lokasi'],
			'jumlah'	 	=> $post['jumlah'],
			'spesifikasi' 	=> empty($post['spesifikasi']) ? null : $post['spesifikasi'],
		];
		$this->db->insert('mesin',$params);

	}
	public function edit($post)
	{
		$params = [
			'nama'			=> $post['nama'],
			'model' 	 	=> $post['model'],
			'kondisi' 	 	=> $post['kondisi'],
			'lokasi'	 	=> $post['lokasi'],
			'jumlah'	 	=> $post['jumlah'],
			'spesifikasi' 	=> empty($post['spesifikasi']) ? null : $post['spesifikasi'],
			'updated_at'		=> date('Y-m-d H:i:s')
		];
		$this->db->where('id', $post['id']);
		$this->db->update('mesin',$params);

	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('mesin');
	}


}
