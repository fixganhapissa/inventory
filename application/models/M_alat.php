<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class M_alat extends CI_Model {
	
	public function get($id = null) 
	{
		$this->db->from('alat');
		if($id != null){
			$this->db->where('id', $id); //menampilkan data sesuai parameter where

		}
		$query = $this->db->get();
		return $query;
	}
	public function getAlatData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM alat where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM alat ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getActiveAlatData()
	{
		$sql = "SELECT * FROM alat WHERE is_active = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('alat', $data);
			return ($update == true) ? true : false;
		}
	}

	public function add($post)
	{
		$params = [
			'id'				=> $post['id'],
			'nama'				=> $post['nama'],
			'no_seri' 	 		=> $post['no_seri'],
			'tanggal_kalibrasi' => $post['tanggal_kalibrasi'],
			'program_kalibrasi_ulang' => $post['program_kalibrasi_ulang'],
			'qty' 	 			=> $post['qty'],
			'is_active'			=> 1,
			'kategori' 	 		=> $post['kategori'],
			'keterangan' 		=> empty($post['keterangan']) ? null : $post['keterangan'],
		];
		$this->db->insert('alat',$params);

	}
	public function edit($post)
	{
		$params = [
			'nama'				=> $post['nama'],
			'no_seri' 	 		=> $post['no_seri'],
			'tanggal_kalibrasi' => $post['tanggal_kalibrasi'],
			'program_kalibrasi_ulang' => $post['program_kalibrasi_ulang'],
			'qty' 				=> $post['qty'],
			'kategori'			=> $post['kategori'],
			'keterangan' 		=> empty($post['keterangan']) ? null : $post['keterangan'],
			'updated_at'		=> date('Y-m-d H:i:s')
		];
		$this->db->where('id', $post['id']);
		$this->db->update('alat',$params);

	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('alat');
	}

}
