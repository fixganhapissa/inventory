<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_masuk extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_barang_masuk');
		$this->load->model('M_barang');
        $this->load->library('form_validation');

	}

	public function index()
	{	
		$this->template->load('template','barang_masuk/data');
	}

    public function fetchBarangMasukData()
    {
        $result = array('data' => array());
        $data = $this->M_barang_masuk->getBarangMasukData();
        foreach ($data as $key => $value) {

            $buttons = '';
            $dropdown = '';

            $buttons .= '<a href="'.base_url('barang_masuk/update/'.$value['id']).'" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Edit"><i class="icon icon-left mdi mdi-edit"></i></a>';
            $buttons .= ' <button type="button" class="btn btn-default" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal"><i class="icon icon-left mdi mdi-delete"></i></button>';

            $dropdown.= '
                <div class="btn-group btn-hspace">
                    <button class="btn-default btn" type="button" tabindex="-1" data-provider="bootstrap-markdown" data-toggle="dropdown" data-handler="bootstrap-markdown-cmdList" d><span class="glyphicon glyphicon-list"></span> </button>

                    <ul role="menu" class="dropdown-menu">
                        <li><a href="'.base_url('barang_masuk/update/'.$value['id']).'">Edit</a></li>
                        <li><a href="#" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal">Hapus</a></li>
                        <li class="divider"></li>
                        <li><a href="'.base_url('barang_masuk/detail/'.$value['id']).'">Detail</a></li>
                    </ul>
                </div>';

            $result['data'][$key] = array(
                $value['inv_no'],
                $value['nama'],
                $value['no_telp'],
                $value['proyek'],
                $value['tanggal_penyerahan'],
                $dropdown
            );
        } // /foreach
        echo json_encode($result);
    }

	public function create()
    {
        $this->form_validation->set_rules('nama', 'Nama Pengirim', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No. Telp./HP', 'trim|required');
        $this->form_validation->set_rules('proyek', 'Proyek', 'trim|required');
        $this->form_validation->set_rules('barang[]', 'Nama Barang', 'trim|required');
        $this->form_validation->set_rules('keterangan[]', 'Keterangan', 'trim|required');

        if ($this->form_validation->run() == TRUE) {            
            
            $id_barang_masuk = $this->M_barang_masuk->create();
            
            if($id_barang_masuk) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan create');
                redirect('barang_masuk/update/'.$id_barang_masuk, 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error !!');
                redirect('barang_masuk/create/', 'refresh');
            }
        }
        else {
            // false case

            $this->data['barang'] = $this->M_barang->getActiveBarangData();        
            $this->template->load('template','barang_masuk/create',$this->data);
        }   
    }

    /*
    * mendapatkan id barang dari ajax method.
    * memeriksa, mengambil data barang tertentu dari table barang 
    * return data ke dalam json format.
    */
    public function getBarangValueById()
    {
        $barang_id = $this->input->post('barang_id');
        if($barang_id) {
            $barang_data = $this->M_barang->getBarangData($barang_id);
            echo json_encode($barang_data);
        }
    }

    /*
    * get semua data informasi barang dari tabel barang
    * response di return ke json format.
    */
    public function getTableBarangRow()
    {
        $barang = $this->M_barang->getActiveBarangData();
        echo json_encode($barang);
    }

    public function update($id)
    {   

        $this->form_validation->set_rules('nama', 'Nama Pengirim', 'required');
        // $this->form_validation->set_rules('no_telp', 'No. Telp./HP', 'trim|required');
        // $this->form_validation->set_rules('proyek', 'Proyek', 'trim|required');
        // $this->form_validation->set_rules('barang[]', 'Nama Barang', 'trim|required');
        // $this->form_validation->set_rules('keterangan[]', 'Keterangan', 'trim|required');
        $id_barang_masuk = $id;
        if ($this->form_validation->run() == TRUE) {     
            $update = $this->M_barang_masuk->update($id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('barang_masuk/update/'.$id);
            }
            else {
                $this->session->set_flashdata('errors', 'Error');
                redirect('barang_masuk/update/'.$id);
            }
        }
        else {
    
        // false case
            $result = array();
            $data_barang_masuk = $this->M_barang_masuk->getBarangMasukData($id);

            $result['barang_masuk'] = $data_barang_masuk;
            $detail_barang_masuk = $this->M_barang_masuk->getDetailBarangMasukData($data_barang_masuk['id']);

            foreach($detail_barang_masuk as $k => $v) {
                $result['detail_barang_masuk'][] = $v;
            }

            $this->data['data_barang_masuk'] = $result;

            $this->data['barang'] = $this->M_barang->getActiveBarangData();        
            $this->template->load('template','barang_masuk/edit',$this->data);    
        }
    }

    public function detail($id)
    {   
        $result = array();
        $data_barang_masuk = $this->M_barang_masuk->getBarangMasukData($id);

        $result['barang_masuk'] = $data_barang_masuk;
        $detail_barang_masuk = $this->M_barang_masuk->getDetailBarangMasukData($data_barang_masuk['id']);

        foreach($detail_barang_masuk as $k => $v) {
            $result['detail_barang_masuk'][] = $v;
        }

        $this->data['data_barang_masuk'] = $result;
        $this->data['barang'] = $this->M_barang->getActiveBarangData();        
        $this->template->load('template','barang_masuk/detail',$this->data);   
    }

    public function remove()
    {
        $id_barang_masuk = $this->input->post('id_barang_masuk');

        $response = array();
        if($id_barang_masuk) {
            $delete = $this->M_barang_masuk->remove($id_barang_masuk);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Berhasil dihapus"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error database ketika remove data barang";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh halaman!!";
        }

        echo json_encode($response); 
    }


	
}
