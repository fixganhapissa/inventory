<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_barang extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
        $this->load->model('M_barang');
        $this->load->model('M_stok_barang');
	}

	public function index()
    {   
        $data['row'] = $this->M_barang->get();
        $this->template->load('template','stok/stok_barang',$data);
    }

    public function detail($id)
    {   
        $result = array();
        $data_barang = $this->M_stok_barang->getBarangData($id);
        $result['barang'] = $data_barang;

        $data_barang_keluar = $this->M_stok_barang->getBarangKeluarData($id);
        $result['barang_keluar'] = $data_barang_keluar;

        $detail_barang_keluar = $this->M_stok_barang->getDetailBarangKeluarData($data_barang['id']);

        foreach($detail_barang_keluar as $k => $v) {
            $result['detail_barang_keluar'][] = $v;
        }

        $this->data['data_barang'] = $result;
        $this->data['data_barang_keluar'] = $result;
        $this->data['barang'] = $this->M_barang->getActiveBarangData();        
        $this->template->load('template','stok/detail_stok_barang',$this->data);   
    }

   
}
