<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesin extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_mesin');
	}

	public function index()
	{	
		$data['row'] = $this->M_mesin->get();
		$this->template->load('template','mesin/data',$data);
	}

	public function add()
	{	
		$mesin = new stdClass();
		$mesin->id = null;
		$mesin->nama = null;
		$mesin->model = null;
		$mesin->kondisi = null;
		$mesin->lokasi = null;
		$mesin->jumlah = null;
		$mesin->spesifikasi = null;
		$data = array(
			'page'=> 'add',
			'row' => $mesin
			);

		$this->template->load('template','mesin/form',$data);
	}

	public function edit($id)
	{
		$query = $this->M_mesin->get($id);
		if ($query->num_rows() > 0) {
			$mesin = $query->row();
			$data = array(
				'page'=> 'edit',
				'row' => $mesin
			);
			$this->template->load('template','mesin/form',$data);
		} else {
			echo "<script>alert('Data tidak ditemukan');</script>";
			redirect('mesin');
		}
	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$this->M_mesin->add($post);
		} else if (isset($_POST['edit']))
			$this->M_mesin->edit($post);

		if ($this->db->affected_rows()>0) {
			$this->session->set_flashdata('success','Data berhasil disimpan');
		}
		redirect('mesin');
		
	}

	public function del($id)
	{
		$this->M_mesin->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('mesin');
	} 
}
