<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat_pantau extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_alat_pantau');
	}

	public function index()
	{	
		$data['row'] = $this->M_alat_pantau->get();
		$this->template->load('template','alat_pantau/data',$data);
	}

	public function add()
	{	
		$alat_pantau = new stdClass();
		$alat_pantau->id = null;
		$alat_pantau->nama = null;
		$alat_pantau->no_seri = null;
		$alat_pantau->tanggal_kalibrasi = null;
		$alat_pantau->program_kalibrasi_ulang = null;
		$alat_pantau->qty = null;
		$alat_pantau->keterangan = null;
		$data = array(
			'page'=> 'add',
			'row' => $alat_pantau
			);

		$this->template->load('template','alat_pantau/form',$data);
	}

	public function edit($id)
	{
		$query = $this->M_alat_pantau->get($id);
		if ($query->num_rows() > 0) {
			$alat_pantau = $query->row();
			$data = array(
				'page'=> 'edit',
				'row' => $alat_pantau
			);
			$this->template->load('template','alat_pantau/form',$data);
		} else {
			echo "<script>alert('Data tidak ditemukan');</script>";
			redirect('alat_pantau');
		}
	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$this->M_alat_pantau->add($post);
		} else if (isset($_POST['edit']))
			$this->M_alat_pantau->edit($post);

		if ($this->db->affected_rows()>0) {
			$this->session->set_flashdata('success','Data berhasil disimpan');
		}
		redirect('alat_pantau');
		
	}

	public function del($id)
	{
		$this->M_alat_pantau->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('alat_pantau');
	} 
}
