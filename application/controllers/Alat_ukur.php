<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat_ukur extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_alat_ukur');
	}

	public function index()
	{	
		$data['row'] = $this->M_alat_ukur->get();
		$this->template->load('template','alat_ukur/data',$data);
	}

	public function add()
	{	
		$alat_ukur = new stdClass();
		$alat_ukur->id = null;
		$alat_ukur->nama = null;
		$alat_ukur->no_seri = null;
		$alat_ukur->tanggal_kalibrasi = null;
		$alat_ukur->program_kalibrasi_ulang = null;
		$alat_ukur->qty = null;
		$alat_ukur->keterangan = null;
		$data = array(
			'page'=> 'add',
			'row' => $alat_ukur
			);

		$this->template->load('template','alat_ukur/form',$data);
	}

	public function edit($id)
	{
		$query = $this->M_alat_ukur->get($id);
		if ($query->num_rows() > 0) {
			$alat_ukur = $query->row();
			$data = array(
				'page'=> 'edit',
				'row' => $alat_ukur
			);
			$this->template->load('template','alat_ukur/form',$data);
		} else {
			echo "<script>alert('Data tidak ditemukan');</script>";
			redirect('alat_ukur');
		}
	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$this->M_alat_ukur->add($post);
		} else if (isset($_POST['edit']))
			$this->M_alat_ukur->edit($post);

		if ($this->db->affected_rows()>0) {
			$this->session->set_flashdata('success','Data berhasil disimpan');
		}
		redirect('alat_ukur');
		
	}

	public function del($id)
	{
		$this->M_alat_ukur->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('alat_ukur');
	} 
}
