<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Peminjaman extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_peminjaman');
		$this->load->model('M_alat');
	}

	public function index()
	{	
		$this->template->load('template','peminjaman/data');
	}

    public function fetchPeminjamanData()
    {
        $result = array('data' => array());
        $data = $this->M_peminjaman->getPeminjamanData();
        foreach ($data as $key => $value) {

            $buttons = '';
            $dropdown = '';
            
            $buttons .= ' <a href="'.base_url('peminjaman/update/'.$value['id']).'" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Edit"><i class="icon icon-left mdi mdi-edit"></i></a>';
            $buttons .= ' <button type="button" class="btn btn-default" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal"><i class="icon icon-left mdi mdi-delete"></i></button>';

             $dropdown.= '
                <div class="btn-group btn-hspace">
                    <button class="btn-default btn" type="button" tabindex="-1" data-provider="bootstrap-markdown" data-toggle="dropdown" data-handler="bootstrap-markdown-cmdList" d><span class="glyphicon glyphicon-list"></span> </button>

                    <ul role="menu" class="dropdown-menu">
                        <li><a href="'.base_url('peminjaman/update/'.$value['id']).'">Edit</a></li>
                        <li><a href="#" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal">Hapus</a></li>
                        <li class="divider"></li>
                        <li><a href="'.base_url('peminjaman/pengembalian/'.$value['id']).'">Detail</a></li>
                    </ul>
                </div>';

            if($value['status'] == 1) {
                $status = '<span class="label label-warning">Dipinjam</span>'; 
            }
            else if ($value['status'] == 2) {
                $status = '<span class="label label-success">Dikembalikan</span>';
            }

            $result['data'][$key] = array(
                // $value['inv_no'],
                $value['nama'],
                $value['no_telp'],
                $value['proyek'],
                $value['tanggal_peminjaman'],
                $value['tanggal_pengembalian'],
                $status,
                $dropdown
            );
        } // /foreach
        echo json_encode($result);
    }

	public function create()
    {
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Peminjam', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No. Telp./HP', 'trim|required');
        $this->form_validation->set_rules('proyek', 'Proyek', 'trim|required');
        $this->form_validation->set_rules('alat[]', 'Nama Alat', 'trim|required');
        $this->form_validation->set_rules('status[]', 'Status', 'trim|required');
        $this->form_validation->set_rules('keterangan[]', 'Keterangan', 'trim|required');

        if ($this->form_validation->run() == TRUE) {            
            
            $id_peminjaman = $this->M_peminjaman->create();
            
            if($id_peminjaman) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('peminjaman/update/'.$id_peminjaman, 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error !!');
                redirect('peminjaman/create/', 'refresh');
            }
        }
        else {
            // false case

            $this->data['alat'] = $this->M_alat->getActiveAlatData();   
            $this->template->load('template','peminjaman/create',$this->data);
        }   
    }

    /*
    * mendapatkan id alat dari ajax method.
    * memeriksa, mengambil data alat tertentu dari table alat 
    * return data ke dalam json format.
    */
    public function getAlatValueById()
    {
        $alat_id = $this->input->post('alat_id');
        if($alat_id) {
            $alat_data = $this->M_alat->getAlatData($alat_id);
            echo json_encode($alat_data);
        }
    }

    /*
    * get semua data informasi alat dari tabel alat
    * response di return ke json format.
    */
    public function getTableAlatRow()
    {
        $alat = $this->M_alat->getActiveAlatData();
        echo json_encode($alat);
    }

    public function update($id)
    {   

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Peminjam', 'required');

        $id_peminjaman = $id;
        if ($this->form_validation->run() == TRUE) {     
            $update = $this->M_peminjaman->update($id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('peminjaman/update/'.$id);
            }
            else {
                $this->session->set_flashdata('errors', 'Error');
                redirect('peminjaman/update/'.$id);
            }
        }
        else {
    
        // false case
            $result = array();
            $data_peminjaman = $this->M_peminjaman->getPeminjamanData($id);

            $result['peminjaman'] = $data_peminjaman;
            $detail_peminjaman = $this->M_peminjaman->getDetailPeminjamanData($data_peminjaman['id']);

            foreach($detail_peminjaman as $k => $v) {
                $result['detail_peminjaman'][] = $v;
            }

            $this->data['data_peminjaman'] = $result;

            $this->data['alat'] = $this->M_alat->getActiveAlatData();        
            $this->template->load('template','peminjaman/edit',$this->data);    
        }
    }

    public function pengembalian($id)
    {   
        $this->load->library('form_validation');
        $this->form_validation->set_rules('status', 'Status', 'required');

        $id_peminjaman = $id;
        if ($this->form_validation->run() == TRUE) {     
            $update = $this->M_peminjaman->pengembalian($id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('peminjaman/pengembalian/'.$id);
            }
            else {
                $this->session->set_flashdata('errors', 'Error');
                redirect('peminjaman/pengembalian/'.$id);
            }
        }
        else {
        // false case
            $result = array();
            $data_peminjaman = $this->M_peminjaman->getPeminjamanData($id);

            $result['peminjaman'] = $data_peminjaman;
            $detail_peminjaman = $this->M_peminjaman->getDetailPeminjamanData($data_peminjaman['id']);

            foreach($detail_peminjaman as $k => $v) {
                $result['detail_peminjaman'][] = $v;
            }

            $this->data['data_peminjaman'] = $result;
            $this->data['alat'] = $this->M_alat->getActiveAlatData();        
            $this->template->load('template','pengembalian/pengembalian',$this->data);    
        }
    }

    public function remove()
    {
        $id_peminjaman = $this->input->post('id_peminjaman');

        $response = array();
        if($id_peminjaman) {
            $delete = $this->M_peminjaman->remove($id_peminjaman);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Berhasil dihapus"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error database ketika remove data alat";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh halaman!!";
        }

        echo json_encode($response); 
    }	
}
