<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_barang');
	}

	public function index()
	{	
		$data['row'] = $this->M_barang->get();
		$this->template->load('template','barang/data',$data);
	}

	public function add()
	{	
		$barang = new stdClass();
		$barang->id = null;
		$barang->nama = null;
		$barang->type = null;
		$barang->vendor = null;
		$barang->qty = null;
		$barang->deskripsi = null;
		$data = array(
			'page'=> 'add',
			'row' => $barang
			);

		$this->template->load('template','barang/form',$data);
	}

	public function edit($id)
	{
		$query = $this->M_barang->get($id);
		if ($query->num_rows() > 0) {
			$barang = $query->row();
			$data = array(
				'page'=> 'edit',
				'row' => $barang
			);
			$this->template->load('template','barang/form',$data);
		} else {
			echo "<script>alert('Data tidak ditemukan');</script>";
			redirect('barang');
		}
	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$this->M_barang->add($post);
		} else if (isset($_POST['edit']))
			$this->M_barang->edit($post);

		if ($this->db->affected_rows()>0) {
			$this->session->set_flashdata('success','Data berhasil disimpan');
		}
		redirect('barang');
		
	}

	public function del($id)
	{
		$this->M_barang->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('barang');
	} 
}
