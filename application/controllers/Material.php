<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Material extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_material');
	}

	public function index()
	{	
		$data['row'] = $this->M_material->get();
		$this->template->load('template','material/data',$data);
	}

	public function add()
	{	
		$material = new stdClass();
		$material->id = null;
		$material->nama = null;
		$material->model = null;
		$material->kondisi = null;
		$material->lokasi = null;
		$material->qty = null;
		$material->spesifikasi = null;
		$data = array(
			'page'=> 'add',
			'row' => $material
			);

		$this->template->load('template','material/form',$data);
	}
	
	public function edit($id)
	{
		$query = $this->M_material->get($id);
		if ($query->num_rows() > 0) {
			$material = $query->row();
			$data = array(
				'page'=> 'edit',
				'row' => $material
			);
			$this->template->load('template','material/form',$data);
		} else {
			echo "<script>alert('Data tidak ditemukan');</script>";
			redirect('material');
		}
	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$this->M_material->add($post);
		} else if (isset($_POST['edit']))
			$this->M_material->edit($post);

		if ($this->db->affected_rows()>0) {
			$this->session->set_flashdata('success','Data berhasil disimpan');
		}
		redirect('material');
		
	}

	public function del($id)
	{
		$this->M_material->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('material');
	} 
}
