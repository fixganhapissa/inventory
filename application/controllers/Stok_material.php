<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_material extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
        $this->load->model('M_material');
        $this->load->model('M_stok_material');
	}

	public function index()
    {   
        $data['row'] = $this->M_material->get();
        $this->template->load('template','stok/stok_material',$data);
    }

    public function detail($id)
    {   
        $result = array();
        $data_material = $this->M_stok_material->getMaterialData($id);
        $result['material'] = $data_material;

        $data_permintaan = $this->M_stok_material->getPermintaanData($id);
        $result['barang_keluar'] = $data_permintaan;

        $detail_permintaan = $this->M_stok_material->getDetailPermintaanData($data_material['id']);

        foreach($detail_permintaan as $k => $v) {
            $result['detail_permintaan'][] = $v;
        }

        $this->data['data_material'] = $result;
        $this->data['data_permintaan'] = $result;
        $this->data['material'] = $this->M_material->getActiveMaterialData();        
        $this->template->load('template','stok/detail_stok_material',$this->data);   
    }

   
}
