<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	// fungsi yang di panggil pertamakali __construct
	public function __construct()
	{	
		parent::__construct();
		hak_akses();
		belumlogin();
		$this->load->model('M_user');
		$this->load->library('form_validation');
	}

	public function index()
	{	
		$data['row'] = $this->M_user->get();
		$this->template->load('template','user/user',$data);	
	}


	public function add()
	{
		$this->form_validation->set_rules('nama','Nama Lengkap','required');
		$this->form_validation->set_rules('telepon','Telepon','required');
		$this->form_validation->set_rules('email','Email','required');

		$this->form_validation->set_rules('username','Username','required|min_length[6]|is_unique[user.username]');
		$this->form_validation->set_rules('password','Password','required|min_length[6]');
		$this->form_validation->set_rules('passconf','Konfirmasi Password','required|min_length[6]|matches[password]', 
			array('matches' => '%s tidak sama')
			);
		$this->form_validation->set_rules('level','Level','required');

		$this->form_validation->set_message('required','%s silahkan isi');
		$this->form_validation->set_message('min_length', '{field} minimal 6 karakter');
		$this->form_validation->set_message('is_unique', '{field} ini sudah dipakai, silahkan ganti');

		$this->form_validation->set_error_delimiters('<span class="help-block">','</span>');

		if ($this->form_validation->run() == FALSE) {
			$this->template->load('template','user/user_add');
		} else{
			// proses simpan data
			$post = $this->input->post(null, TRUE);
			$this->M_user->add($post);
			if ($this->db->affected_rows() > 0) {
				echo "<script>alert('Data Berhasil disimpan');</script>";
			}
			echo "<script>window.location='".base_url('user')."';</script>";
			// redirect(base_url('user'));
		}
	}

		public function edit($id)
	{
		$this->form_validation->set_rules('nama','Nama Lengkap','required');
		$this->form_validation->set_rules('telepon','Telepon','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('username','Username','required|callback_username_check');
		if ($this->input->post('password')) {
			$this->form_validation->set_rules('password','Password','min_length[5]');
			$this->form_validation->set_rules('passconf','Konfirmasi Password','|matches[password]', 
			array('matches' => '%s tidak sama')
			);
		}

		if ($this->input->post('passconf')) {
			$this->form_validation->set_rules('passconf','Konfirmasi Password','matches[password]', 
				array('matches' => '%s tidak sama')
			);
		}
		
		$this->form_validation->set_rules('level','Level','required');

		$this->form_validation->set_message('required','%s silahkan isi');
		$this->form_validation->set_message('min_length', '{field} minimal 6 karakter');
		$this->form_validation->set_message('is_unique', '{field} ini sudah dipakai, silahkan ganti');

		$this->form_validation->set_error_delimiters('<span class="help-block">','</span>');

		if ($this->form_validation->run() == FALSE) {
			$query = $this->M_user->get($id);
			if ($query->num_rows()>0) {
				$data['row'] = $query->row();
				$this->template->load('template','user/user_edit', $data);			
			} else {
				echo "<script>alert('Data tidak ditemukan');</script>";
				echo "<script>window.location='".base_url('user')."';</script>";
			}
			
		} else{
			// proses simpan data
			$post = $this->input->post(null, TRUE);
			$this->M_user->edit($post);
			if ($this->db->affected_rows() > 0) {
				echo "<script>alert('Data Berhasil disimpan');</script>";
			}
			echo "<script>window.location='".base_url('user')."';</script>";
			// redirect(base_url('user'));
		}
	}

	public function username_check()
	{
		$post = $this->input->post(null, TRUE);
		$query = $this->db->query("SELECT * FROM user WHERE username = '$post[username]' AND id_user != '$post[id_user]' ");
		if ($query->num_rows() > 0) {
			$this->form_validation->set_message('username_check', '{field} ini sudah dipakai, silahkan ganti');
			return FALSE;
		} else{
			return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id_user');
		$this->M_user->del($id);

		if ($this->db->affected_rows() > 0) {
			echo "<script>alert('Data Berhasil dihapus');</script>";
		}
		echo "<script>window.location='".base_url('user')."';</script>";
	} 
}
