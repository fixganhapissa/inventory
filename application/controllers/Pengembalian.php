<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Pengembalian extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_pengembalian');
		$this->load->model('M_material');
        $this->load->library('pdf');
	}

	public function index()
	{	
		$this->template->load('template','pengembalian/data');
	}

    public function fetchPengembalianData()
    {
        $result = array('data' => array());
        $data = $this->M_pengembalian->getPengembalianData();
        foreach ($data as $key => $value) {

            $buttons = '';
            
            $buttons .= ' <a href="'.base_url('pengembalian/update/'.$value['id']).'" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Edit"><i class="icon icon-left mdi mdi-edit"></i></a>';
            $buttons .= ' <button type="button" class="btn btn-default" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal"><i class="icon icon-left mdi mdi-delete"></i></button>';

            $result['data'][$key] = array(
                $value['nama'],
                $value['no_telp'],
                $value['proyek'],
                $value['tanggal_pengembalian'],
                $buttons
            );
        } // /foreach
        echo json_encode($result);
    }

	public function create()
    {
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Lengkap Peminta', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No. Telp./HP', 'trim|required');
        $this->form_validation->set_rules('proyek', 'Proyek', 'trim|required');
        $this->form_validation->set_rules('material[]', 'Nama Material', 'trim|required');
        $this->form_validation->set_rules('keterangan[]', 'Keterangan', 'trim|required');

        if ($this->form_validation->run() == TRUE) {            
            
            $id_pengembalian = $this->M_pengembalian->create();
            
            if($id_pengembalian) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('pengembalian/update/'.$id_pengembalian, 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error !!');
                redirect('pengembalian/create/', 'refresh');
            }
        }
        else {
            // false case

            $this->data['material'] = $this->M_material->getActiveMaterialData();        
            $this->template->load('template','pengembalian/create',$this->data);
        }   
    }

    /*
    * mendapatkan id pengembalian dari ajax method.
    * memeriksa, mengambil data pengembalian tertentu dari table pengembalian 
    * return data ke dalam json format.
    */
    public function getPengembalianValueById()
    {
        $material_id = $this->input->post('material_id');
        if($material_id) {
            $material_data = $this->M_material->getMaterialData($material_id);
            echo json_encode($material_data);
        }
    }

    /*
    * get semua data informasi material dari tabel material
    * response di return ke json format.
    */
    public function getTableMaterialRow()
    {
        $material = $this->M_material->getActiveMaterialData();
        echo json_encode($material);
    }

    public function update($id)
    {   

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Pengirim', 'required');

        $id_pengembalian = $id;
        if ($this->form_validation->run() == TRUE) {     
            $update = $this->M_pengembalian->update($id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('pengembalian/update/'.$id);
            }
            else {
                $this->session->set_flashdata('errors', 'Error');
                redirect('pengembalian/update/'.$id);
            }
        }
        else {
    
        // false case
            $result = array();
            $data_pengembalian = $this->M_pengembalian->getPengembalianData($id);

            $result['pengembalian'] = $data_pengembalian;
            $detail_pengembalian = $this->M_pengembalian->getDetailPengembalianData($data_pengembalian['id']);

            foreach($detail_pengembalian as $k => $v) {
                $result['detail_pengembalian'][] = $v;
            }

            $this->data['data_pengembalian'] = $result;

            $this->data['material'] = $this->M_material->getActiveMaterialData();        
            $this->template->load('template','pengembalian/edit',$this->data);    
        }
    }

    public function remove()
    {
        $id_pengembalian = $this->input->post('id_pengembalian');

        $response = array();
        if($id_pengembalian) {
            $delete = $this->M_pengembalian->remove($id_pengembalian);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Berhasil dihapus"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error database ketika remove data material";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh halaman!!";
        }

        echo json_encode($response); 
    }	
}
