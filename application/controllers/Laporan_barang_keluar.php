<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_barang_keluar extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
        $this->load->model('M_laporan_barang_keluar');
		$this->load->model('M_barang_keluar');
        $this->load->library('pdf');
	}

	public function index()
	{	
		$this->template->load('template','laporan/laporan_barang_keluar');
	}

    public function fetchBarangKeluarData()
    {
        $result = array('data' => array());
        $data = $this->M_barang_keluar->getBarangKeluarData();
        foreach ($data as $key => $value) {

            $buttons = '';

            $buttons .= '<a href="'.base_url('laporan_barang_keluar/cetak/'.$value['id']).'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="icon icon-left mdi mdi-print"></i> Cetak</a>';

            if($value['status'] == 1) {
                $status = '<span class="label label-success">Disetujui</span>'; 
            }
            else if ($value['status'] == 2) {
                $status = '<span class="label label-danger">Tidak Disetujui</span>';
            }
            else{
                $status = '<span class="label label-warning">Menunggu Persetujuan</span>';
            }
            $result['data'][$key] = array(
                $value['inv_no'],
                $value['nama'],
                $value['no_telp'],
                $value['proyek'],
                $value['tanggal_permintaan'],
                $value['tanggal_pengeluaran'],
                $status,
                $buttons
            );
        } // /foreach
        echo json_encode($result);
    }

    public function cetak($id)
    {   
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetTitle('REPORT');
        $data = $this->M_laporan_barang_keluar->cetak_barang_keluar($id);
        $pdf->Image(base_url('assets/img/logo.png'),10,10,-100);
        $pdf->SetFont( 'Arial', '', 10 ); 
        $pdf->Cell( 0, 0, 'ICT', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Simulation & Modelling', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Navigation Telecomunication & Telematry', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Instrument & Control', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Precission Mechatronics', 0, 0, 'R' ); 
        $pdf->Line(10, 25, 220-20, 25);
        $pdf->Ln(5);
        $pdf->SetFont('Arial','BU',16); 
        $pdf->Cell(190,7,'REPORT',0,1,'C');
        $pdf->SetFont('Arial','',11);
        foreach ($data as $row){
        $pdf->Cell(40,7,'Invoice Number',0,0, 'L');
        $pdf->Cell(80,7,$row->inv_no,0,0, 'L');
        $pdf->Cell(40,7,'Tanggal Permintaan',0,0, 'L');
        $pdf->Cell(30,7,$row->tanggal_permintaan,0,0, 'R');
        $pdf->Ln(5);
        $pdf->Cell(40,7,'Nama',0,0, 'L');
        $pdf->Cell(80,7,$row->nama,0,0, 'L');
        $pdf->Cell(40,7,'Tanggal Pengeluaran',0,0, 'L');
        $pdf->Cell(30,7,$row->tanggal_pengeluaran,0,0, 'R');
        $pdf->Ln(5);
        $pdf->Cell(40,7,'Proyek',0,0, 'L');
        $pdf->Cell(110,7,$row->proyek,0,0, 'L');
        }
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'NO',1,0, 'C');
        $pdf->Cell(110,6,'DESCRIPTION',1,0, 'C');
        $pdf->Cell(30,6,'TYPE',1,0, 'C');
        $pdf->Cell(30,6,'QTY',1,1, 'C');
        $pdf->SetFont('Arial','',10, 'C');
        $data1 = $this->M_laporan_barang_keluar->cetak_detail_barang($id);
        $no = 1;
        foreach ($data1 as $row1){
            $pdf->Cell(20,6,$no++,1,0,'C');
            $pdf->Cell(110,6,$row1->nama_barang,1,0,'C');
            $pdf->Cell(30,6,$row1->type,1,0,'C');
            $pdf->Cell(30,6,$row1->qty,1,0,'C');
            $pdf->Ln(6);
        }

        $pdf->Ln(15);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(70,7,'Dibuat Oleh',0,1,'L');
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(70,7,'Logistik',0,1,'L');
        $pdf->Ln(20);
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(70,7,'...............................',0,1,'L');
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(20,7,'Tanggal :',0,0,'L');
        $pdf->Cell(50,7,date("d-m-Y"),0,1,'L');

        $pdf->Output();
    }	
}
