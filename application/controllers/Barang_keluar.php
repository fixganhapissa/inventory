<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Barang_keluar extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_barang_keluar');
		$this->load->model('M_barang');
        $this->load->library('pdf');
	}

	public function index()
	{	
		$this->template->load('template','barang_keluar/data');
	}

    public function fetchBarangKeluarData()
    {
        $result = array('data' => array());
        $data = $this->M_barang_keluar->getBarangKeluarData();
        foreach ($data as $key => $value) {

            $buttons = '';
            $dropdown = '';
            
            if($value['status'] == 1) {
                $buttons .= '<a target="__blank" href="'.base_url('barang_keluar/surat_jalan/'.$value['id']).'" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Surat Jalan"><i class="icon icon-left mdi mdi-truck"></i></a>';
            }
            $buttons .= ' <a href="'.base_url('barang_keluar/update/'.$value['id']).'" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Edit"><i class="icon icon-left mdi mdi-edit"></i></a>';
            $buttons .= ' <button type="button" class="btn btn-default" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal"><i class="icon icon-left mdi mdi-delete"></i></button>';

            $dropdown.= '
                <div class="btn-group btn-hspace">
                    <button class="btn-default btn" type="button" tabindex="-1" data-provider="bootstrap-markdown" data-toggle="dropdown" data-handler="bootstrap-markdown-cmdList" d><span class="glyphicon glyphicon-list"></span> </button>

                    <ul role="menu" class="dropdown-menu">
                        <li><a href="'.base_url('barang_keluar/update/'.$value['id']).'">Edit</a></li>
                        <li><a href="#" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal">Hapus</a></li>
                        <li class="divider"></li>
                        <li><a href="'.base_url('barang_keluar/detail/'.$value['id']).'">Detail</a></li>';
                        if($value['status'] == 1) {
                        $dropdown.='<li><a href="'.base_url('barang_keluar/surat_jalan/'.$value['id']).'">Surat Jalan</a></li>';
                        }
                    '</ul>
                </div>';

            if($value['status'] == 1) {
                $status = '<span class="label label-success">Disetujui</span>'; 
            }
            else if ($value['status'] == 2) {
                $status = '<span class="label label-danger">Tidak Disetujui</span>';
            }
            else{
                $status = '<span class="label label-warning">Menunggu Persetujuan</span>';
            }
            $result['data'][$key] = array(
                $value['inv_no'],
                $value['nama'],
                $value['no_telp'],
                $value['proyek'],
                $value['tanggal_permintaan'],
                $value['tanggal_pengeluaran'],
                $status,
                $dropdown
            );
        } // /foreach
        echo json_encode($result);
    }

	public function create()
    {
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Pengirim', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No. Telp./HP', 'trim|required');
        $this->form_validation->set_rules('proyek', 'Proyek', 'trim|required');
        $this->form_validation->set_rules('barang[]', 'Nama Barang', 'trim|required');
        $this->form_validation->set_rules('keterangan[]', 'Keterangan', 'trim|required');

        if ($this->form_validation->run() == TRUE) {            
            
            $id_barang_keluar = $this->M_barang_keluar->create();
            
            if($id_barang_keluar) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('barang_keluar/update/'.$id_barang_keluar, 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error !!');
                redirect('barang_keluar/create/', 'refresh');
            }
        }
        else {
            // false case

            $this->data['barang'] = $this->M_barang->getActiveBarangData();        
            $this->template->load('template','barang_keluar/create',$this->data);
        }   
    }

    /*
    * mendapatkan id barang dari ajax method.
    * memeriksa, mengambil data barang tertentu dari table barang 
    * return data ke dalam json format.
    */
    public function getBarangValueById()
    {
        $barang_id = $this->input->post('barang_id');
        if($barang_id) {
            $barang_data = $this->M_barang->getBarangData($barang_id);
            echo json_encode($barang_data);
        }
    }

    /*
    * get semua data informasi barang dari tabel barang
    * response di return ke json format.
    */
    public function getTableBarangRow()
    {
        $barang = $this->M_barang->getActiveBarangData();
        echo json_encode($barang);
    }

    public function update($id)
    {   

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Pengirim', 'required');

        $id_barang_keluar = $id;
        if ($this->form_validation->run() == TRUE) {     
            $update = $this->M_barang_keluar->update($id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('barang_keluar/update/'.$id);
            }
            else {
                $this->session->set_flashdata('errors', 'Error');
                redirect('barang_keluar/update/'.$id);
            }
        }
        else {
    
        // false case
            $result = array();
            $data_barang_keluar = $this->M_barang_keluar->getBarangKeluarData($id);

            $result['barang_keluar'] = $data_barang_keluar;
            $detail_barang_keluar = $this->M_barang_keluar->getDetailBarangKeluarData($data_barang_keluar['id']);

            foreach($detail_barang_keluar as $k => $v) {
                $result['detail_barang_keluar'][] = $v;
            }

            $this->data['data_barang_keluar'] = $result;

            $this->data['barang'] = $this->M_barang->getActiveBarangData();        
            $this->template->load('template','barang_keluar/edit',$this->data);    
        }
    }

    public function remove()
    {
        $id_barang_keluar = $this->input->post('id_barang_keluar');

        $response = array();
        if($id_barang_keluar) {
            $delete = $this->M_barang_keluar->remove($id_barang_keluar);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Berhasil dihapus"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error database ketika remove data barang";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh halaman!!";
        }

        echo json_encode($response); 
    }

    public function detail($id)
    {      
        // var_dump($id);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('status', 'Status', 'required');

        $id_barang_keluar = $id;
        if ($this->form_validation->run() == TRUE) {     
            $update = $this->M_barang_keluar->persetujuan($id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('barang_keluar/detail/'.$id);
            }
            else {
                $this->session->set_flashdata('errors', 'Error');
                redirect('barang_keluar/detail/'.$id);
            }
        }
        else {
        // false case
        $result = array();
        $data_barang_keluar = $this->M_barang_keluar->getBarangKeluarData($id);

        $result['barang_keluar'] = $data_barang_keluar;
        $detail_barang_keluar = $this->M_barang_keluar->getDetailBarangKeluarData($data_barang_keluar['id']);

        foreach($detail_barang_keluar as $k => $v) {
            $result['detail_barang_keluar'][] = $v;
        }

        $this->data['data_barang_keluar'] = $result;
        $this->data['barang'] = $this->M_barang->getActiveBarangData();        
        $this->template->load('template','barang_keluar/detail',$this->data); 
        }

         
    }

    public function surat_jalan($id){

        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetTitle('Surat Jalan');

        $pdf->Image(base_url('assets/img/logo.png'),10,10,-100);
        $pdf->SetFont( 'Arial', '', 10 ); 
        $pdf->Cell( 0, 0, 'ICT', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Simulation & Modelling', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Navigation Telecomunication & Telematry', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Instrument & Control', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Precission Mechatronics', 0, 0, 'R' ); 
        $pdf->Line(10, 25, 220-20, 25);
        $pdf->Ln(5);
        $pdf->SetFont('Arial','BU',16); 
        $pdf->Cell(190,7,'SURAT JALAN',0,1,'C');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(190,7,'No                 : .../SJ/TMU/.../201...',0,1,'L');
        $pdf->Cell(190,7,'Tanggal         : .....................201...',0,1,'L');
        $pdf->Cell(190,7,'No. Kontrak   : ..............................',0,1,'L');
        $pdf->Cell(0,6,'Kepada Yth.',1,10);
        $pdf->Cell(0,25,'',1,10);
        $pdf->Cell(60,6,'Kendaraan',1,0, 'C');
        $pdf->Cell(65,6,'No. Polisi',1,0, 'C');
        $pdf->Cell(65,6,'Sopir',1,1, 'C');
        $pdf->Cell(60,6,'',1,0, 'C');
        $pdf->Cell(65,6,'',1,0, 'C');
        $pdf->Cell(65,6,'',1,1, 'C');
        $pdf->Ln(5);
        $pdf->Cell(190,7,'Dengan ini kami kirimkan sejumlah barang sebagai berikut :',0,1,'L');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'NO',1,0, 'C');
        $pdf->Cell(110,6,'DESCRIPTION',1,0, 'C');
        $pdf->Cell(30,6,'QTY',1,0, 'C');
        $pdf->Cell(30,6,'UNIT',1,1, 'C');
        $pdf->SetFont('Arial','',10, 'C');
        $data = $this->M_barang_keluar->surat_jalan($id);
        $no = 1;
        foreach ($data as $row){
            $pdf->Cell(20,6,$no++,1,0,'C');
            $pdf->Cell(110,6,$row->nama_barang,1,0,'C');
            $pdf->Cell(30,6,$row->qty,1,0,'C');
            $pdf->Cell(30,6,$row->qty,1,0,'C');
        }

        $pdf->Ln(15);
        $pdf->Cell(190,7,'Mohon Untuk di cek dan diterima.',0,1,'L');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(90,7,'Penerima',0,0,'L');
        $pdf->Cell(100,7,'Pengirim',0,1,'C');
        $pdf->Cell(90,7,'................................',0,0,'L');
        $pdf->Cell(100,7,'PT TECHNO MULTI UTAMA',0,1,'C');
        $pdf->Ln(20);
        $pdf->Cell(90,7,'................................',0,0,'L');
        $pdf->Cell(100,7,'...............................',0,1,'C');

        $pdf->Output();
    }

}
