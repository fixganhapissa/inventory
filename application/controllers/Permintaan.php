<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Permintaan extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_permintaan');
		$this->load->model('M_material');
        $this->load->library('pdf');
	}

	public function index()
	{	
		$this->template->load('template','permintaan/data');
	}

    public function fetchPermintaanData()
    {
        $result = array('data' => array());
        $data = $this->M_permintaan->getPermintaanData();
        foreach ($data as $key => $value) {

            $buttons = '';
            
            $buttons .= ' <a href="'.base_url('permintaan/update/'.$value['id']).'" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Edit"><i class="icon icon-left mdi mdi-edit"></i></a>';
            $buttons .= ' <button type="button" class="btn btn-default" onclick="removeFunc('.$value['id'].')"  data-toggle="modal" data-target="#removeModal"><i class="icon icon-left mdi mdi-delete"></i></button>';

            $result['data'][$key] = array(
                $value['nama'],
                $value['no_telp'],
                $value['proyek'],
                $value['tanggal_permintaan'],
                $value['tanggal_penyerahan'],
                $buttons
            );
        } // /foreach
        echo json_encode($result);
    }

	public function create()
    {
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Lengkap Peminta', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No. Telp./HP', 'trim|required');
        $this->form_validation->set_rules('proyek', 'Proyek', 'trim|required');
        $this->form_validation->set_rules('material[]', 'Nama Material', 'trim|required');
        $this->form_validation->set_rules('keterangan[]', 'Keterangan', 'trim|required');

        if ($this->form_validation->run() == TRUE) {            
            
            $id_permintaan = $this->M_permintaan->create();
            
            if($id_permintaan) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('permintaan/update/'.$id_permintaan, 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error !!');
                redirect('permintaan/create/', 'refresh');
            }
        }
        else {
            // false case

            $this->data['material'] = $this->M_material->getActiveMaterialData();        
            $this->template->load('template','permintaan/create',$this->data);
        }   
    }

    /*
    * mendapatkan id permintaan dari ajax method.
    * memeriksa, mengambil data permintaan tertentu dari table permintaan 
    * return data ke dalam json format.
    */
    public function getPermintaanValueById()
    {
        $material_id = $this->input->post('material_id');
        if($material_id) {
            $material_data = $this->M_material->getMaterialData($material_id);
            echo json_encode($material_data);
        }
    }

    /*
    * get semua data informasi material dari tabel material
    * response di return ke json format.
    */
    public function getTableMaterialRow()
    {
        $material = $this->M_material->getActiveMaterialData();
        echo json_encode($material);
    }

    public function update($id)
    {   

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Pengirim', 'required');

        $id_permintaan = $id;
        if ($this->form_validation->run() == TRUE) {     
            $update = $this->M_permintaan->update($id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('permintaan/update/'.$id);
            }
            else {
                $this->session->set_flashdata('errors', 'Error');
                redirect('permintaan/update/'.$id);
            }
        }
        else {
    
        // false case
            $result = array();
            $data_permintaan = $this->M_permintaan->getPermintaanData($id);

            $result['permintaan'] = $data_permintaan;
            $detail_permintaan = $this->M_permintaan->getDetailPermintaanData($data_permintaan['id']);

            foreach($detail_permintaan as $k => $v) {
                $result['detail_permintaan'][] = $v;
            }

            $this->data['data_permintaan'] = $result;

            $this->data['material'] = $this->M_material->getActiveMaterialData();        
            $this->template->load('template','permintaan/edit',$this->data);    
        }
    }

    public function remove()
    {
        $id_permintaan = $this->input->post('id_permintaan');

        $response = array();
        if($id_permintaan) {
            $delete = $this->M_permintaan->remove($id_permintaan);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Berhasil dihapus"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error database ketika remove data material";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh halaman!!";
        }

        echo json_encode($response); 
    }	
}
