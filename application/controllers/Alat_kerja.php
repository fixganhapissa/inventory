<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat_kerja extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_alat_kerja');
	}

	public function index()
	{	
		$data['row'] = $this->M_alat_kerja->get();
		$this->template->load('template','alat_kerja/data',$data);
	}

	public function add()
	{	
		$alat_kerja = new stdClass();
		$alat_kerja->id = null;
		$alat_kerja->nama = null;
		$alat_kerja->no_seri = null;
		$alat_kerja->tanggal_kalibrasi = null;
		$alat_kerja->program_kalibrasi_ulang = null;
		$alat_kerja->qty = null;
		$alat_kerja->keterangan = null;
		$data = array(
			'page'=> 'add',
			'row' => $alat_kerja
			);

		$this->template->load('template','alat_kerja/form',$data);
	}

	public function edit($id)
	{
		$query = $this->M_alat_kerja->get($id);
		if ($query->num_rows() > 0) {
			$alat_kerja = $query->row();
			$data = array(
				'page'=> 'edit',
				'row' => $alat_kerja
			);
			$this->template->load('template','alat_kerja/form',$data);
		} else {
			echo "<script>alert('Data tidak ditemukan');</script>";
			redirect('alat_kerja');
		}
	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$this->M_alat_kerja->add($post);
		} else if (isset($_POST['edit']))
			$this->M_alat_kerja->edit($post);

		if ($this->db->affected_rows()>0) {
			$this->session->set_flashdata('success','Data berhasil disimpan');
		}
		redirect('alat_kerja');
		
	}

	public function del($id)
	{
		$this->M_alat_kerja->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('alat_kerja');
	} 
}
