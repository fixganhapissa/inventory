<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_permintaan_material extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
        $this->load->model('M_laporan_permintaan_material');
		$this->load->model('M_permintaan');
        $this->load->library('pdf');
	}

	public function index()
	{	
		$this->template->load('template','laporan/laporan_permintaan_material');
	}

    public function fetchPermintaanData()
    {
        $result = array('data' => array());
        $data = $this->M_permintaan->getPermintaanData();
        foreach ($data as $key => $value) {

            $buttons = '';

            $buttons .= '<a href="'.base_url('laporan_permintaan_material/cetak/'.$value['id']).'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="icon icon-left mdi mdi-print"></i> Cetak</a>';


            $result['data'][$key] = array(
                $value['nama'],
                $value['no_telp'],
                $value['proyek'],
                $value['tanggal_permintaan'],
                $value['tanggal_penyerahan'],
                $buttons
            );
        } // /foreach
        echo json_encode($result);
    }

    public function cetak($id)
    {   
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetTitle('REPORT');
        $data = $this->M_laporan_permintaan_material->cetak_permintaan($id);
        $pdf->Image(base_url('assets/img/logo.png'),10,10,-100);
        $pdf->SetFont( 'Arial', '', 10 ); 
        $pdf->Cell( 0, 0, 'ICT', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Simulation & Modelling', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Navigation Telecomunication & Telematry', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Instrument & Control', 0, 0, 'R' ); 
        $pdf->Ln(3);
        $pdf->Cell( 0, 0, 'Precission Mechatronics', 0, 0, 'R' ); 
        $pdf->Line(10, 25, 220-20, 25);
        $pdf->Ln(5);
        $pdf->SetFont('Arial','BU',16); 
        $pdf->Cell(190,7,'REPORT',0,1,'C');
        $pdf->SetFont('Arial','',11);
        foreach ($data as $row){
        $pdf->Cell(40,7,'Nama Peminta',0,0, 'L');
        $pdf->Cell(110,7,$row->nama,0,0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(40,7,'Proyek',0,0, 'L');
        $pdf->Cell(110,7,$row->proyek,0,0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(40,7,'Tanggal Permintaan',0,0, 'L');
        $pdf->Cell(110,7,$row->tanggal_permintaan,0,0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(40,7,'Tanggal Penyerahan',0,0, 'L');
        $pdf->Cell(110,7,$row->tanggal_penyerahan,0,0, 'L');
        }
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'NO',1,0, 'C');
        $pdf->Cell(110,6,'DESCRIPTION',1,0, 'C');
        $pdf->Cell(30,6,'TYPE',1,0, 'C');
        $pdf->Cell(30,6,'QTY',1,1, 'C');
        $pdf->SetFont('Arial','',10, 'C');
        $data1 = $this->M_laporan_permintaan_material->cetak_detail_permintaan($id);
        $no = 1;
        foreach ($data1 as $row1){
            $pdf->Cell(20,6,$no++,1,0,'C');
            $pdf->Cell(110,6,$row1->nama_material,1,0,'C');
            $pdf->Cell(30,6,$row1->type,1,0,'C');
            $pdf->Cell(30,6,$row1->qty,1,0,'C');
            $pdf->Ln(6);
        }

        $pdf->Ln(15);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(70,7,'Dibuat Oleh',0,1,'L');
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(70,7,'Logistik',0,1,'L');
        $pdf->Ln(20);
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(70,7,'...............................',0,1,'L');
        $pdf->Cell(130,7,'',0,0,'L');
        $pdf->Cell(20,7,'Tanggal :',0,0,'L');
        $pdf->Cell(50,7,date("d-m-Y"),0,1,'L');

        $pdf->Output();
    }	
}
