<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_alat');
	}

	public function index()
	{	
		$data['row'] = $this->M_alat->get();
		$this->template->load('template','alat/data',$data);
	}

	public function add()
	{	
		$alat = new stdClass();
		$alat->id = null;
		$alat->nama = null;
		$alat->no_seri = null;
		$alat->tanggal_kalibrasi = null;
		$alat->program_kalibrasi_ulang = null;
		$alat->kategori = null;
		$alat->qty = null;
		$alat->keterangan = null;
		$data = array(
			'page'=> 'add',
			'row' => $alat
			);

		$this->template->load('template','alat/form',$data);
	}

	public function edit($id)
	{
		$query = $this->M_alat->get($id);
		if ($query->num_rows() > 0) {
			$alat = $query->row();
			$data = array(
				'page'=> 'edit',
				'row' => $alat
			);
			$this->template->load('template','alat/form',$data);
		} else {
			echo "<script>alert('Data tidak ditemukan');</script>";
			redirect('alat');
		}
	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$this->M_alat->add($post);
		} else if (isset($_POST['edit']))
			$this->M_alat->edit($post);

		if ($this->db->affected_rows()>0) {
			$this->session->set_flashdata('success','Data berhasil disimpan');
		}
		redirect('alat');
		
	}

	public function del($id)
	{
		$this->M_alat->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('alat');
	} 
}
