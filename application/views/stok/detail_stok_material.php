<div class="be-content">
    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-success">
                     <div class="panel-heading panel-heading-divider">Kartu Stok<span class="panel-subtitle"></span></div>
                    <div class="panel-body">

                        <form action="" method="post">

                            <div class="col-md-3">
                                <p><strong>Nama Material</strong></p>
                                <p>Spesifikasi</p>
                            </div>
                            <div class="col-md-6">
                                <p><strong><?php echo $data_material['material']['nama'] ?></strong></p>
                                <p><?php echo $data_material['material']['spesifikasi'] ?></p>
                            </div>

                            <br /> 
                            <br/>
                            <table class="table table-condensed table-hover table-bordered table-striped" id="alat_info_table">
                                <thead class="text-center">
                                    <tr>
                                        <th style="width:5%">No</th>
                                        <th style="width:30%">Tanggal Permintaan</th>
                                        <th style="width:30%">Diambil</th>
                                        <th style="width:30%">Keterangan</th>
                                    </tr>
                                </thead>

                                <tbody class="no-border-x">
                                <?php if(isset($data_permintaan['detail_permintaan'])): ?>
                                    <?php $x = 1; ?>
                                    <?php foreach ($data_permintaan['detail_permintaan'] as $key => $val): ?>
                                    <tr id="row_<?php echo $x; ?>">
                                        <td><?php echo $x++ ; ?></td>
                                        <td><?php echo $val['tanggal_permintaan'] ?></td>
                                        <td><?php echo $val['qty'] ?></td>
                                        <td><?php echo $val['keterangan'] ?></td>
                                    </tr>
                                    <?php $x++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

