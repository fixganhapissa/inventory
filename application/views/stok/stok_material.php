<div class="be-content">

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                    <div class="panel-heading">Stok Material
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Material</th>
                                    <th>Spesifikasi</th>
                                    <th>Model/Type (No. Seri)</th>
                                    <th>Kondisi</th>
                                    <th>Lokasi</th>
                                    <th>Qty</th>
                                    <th>Spesifikasi</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $no = 1;
                                foreach ($row->result() as $key => $data) {
                                # code...
                            ?>
                                <tr class="odd gradeX">
                                   <td><?php echo $no++ ; ?></td>
                                    <td><?php echo $data->nama ; ?></td>
                                    <td><?php echo $data->spesifikasi ; ?></td>
                                    <td><?php echo $data->model ; ?></td>
                                    <td><?php echo $data->kondisi ; ?></td> 
                                    <td><?php echo $data->lokasi ; ?></td> 
                                    <td><?php echo $data->qty ; ?></td> 
                                    <td><?php echo $data->spesifikasi; ?></td>
                                    <td class="text-center">

                                        <a href="<?php echo base_url('stok_material/detail/'.$data->id); ?>">
                                            <button class="btn btn-space btn-primary btn-sm"><i class="icon icon-left mdi mdi-view-dashboard"></i> Detail</button>
                                        </a>                                       
                                    </td>   
                                    
                                </tr> 
                            <?php 
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
