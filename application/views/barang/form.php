<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Barang</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url('barang'); ?>">Barang</a></li>
                <li class="breadcrumb-item active"><?php echo ucfirst($page); ?></li>
            </ol>
        </nav>
    </div>

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                     <div class="panel-heading panel-heading-divider">Form Barang<span class="panel-subtitle"></span></div>
                    <div class="panel-body">

                        <form action="<?php echo base_url('barang/process'); ?>" method="post">
                        <input type="hidden" name="id" value="<?=$row->id?>" >
                            <!-- <div class="form-group col-md-6">
                                <label >Kode</label>
                                <input class="form-control input-sm" type="text" name="id" value="<?=$row->id?>" required <?php echo $page == 'edit' ? "readonly" : '' ; ?>>
                            </div> -->
                            <div class="form-group col-md-6">
                                <label >Nama Barang</label>
                                <input class="form-control input-sm" type="text" name="nama" value="<?=$row->nama?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Type</label>
                                <input class="form-control input-sm" type="text" name="type" value="<?=$row->type?>" required>
                            </div>
                             <div class="form-group col-md-6">
                                <label >Vendor</label>
                                <input class="form-control input-sm" type="text" name="vendor" value="<?=$row->vendor?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Qty</label>
                                <input class="form-control input-sm" type="number" name="qty" value="<?=$row->qty?>" required>
                            </div>
                            <div class="form-group col-md-6">
			                    <label ">Deskripsi</label>
			                    <textarea name="deskripsi" rows="4" class="form-control"><?=$row->deskripsi?></textarea>
		                    </div>
                            <div class="form-group col-md-6"> 
                            </div>
                            
                            <div class="form-group">
	                            <div class="col-sm-4 col-md-12">
	                            <p class="text-right">
	                                <a href="<?php echo base_url('barang'); ?>"><input type="button" class="btn btn-space btn-default" value="Batal"></a>
	                                <button type="submit" name="<?php echo $page ?>" class="btn btn-space btn-primary">Simpan</button> 
	                            </p>
	                            </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>