
<div class="be-content">
	<div class="page-head">
		<h2 class="page-head-title">User</h2>
		<nav aria-label="breadcrumb" role="navigation">
			<ol class="breadcrumb page-head-nav">
				<li class="breadcrumb-item"><a href="<?php echo base_url('user'); ?>">User</a></li>
				<li class="breadcrumb-item active">Data User</li>
			</ol>
		</nav>
	</div>


	<div class="main-content container-fluid">
		<!--Basic Elements-->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-border-color panel-border-color-primary">
					<div class="panel-heading">Data User
						<div class="tools"><a href="<?php echo base_url('user/add'); ?>"><button class="btn btn-primary btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-plus"></i> </span><span class="btn-text">Tambah Data</span></button></a></div>
					</div>
					<div class="panel-body">
						<table class="table table-striped table-hover table-fw-widget" id="table1">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Telepon</th>
									<th>Email</th>
									<th>Level</th>
									<!-- <th>Agama</th> -->
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$no = 1;
								foreach ($row->result() as $key => $data) {
								# code...
							?>
								<tr class="odd gradeX">
									<td><?php echo $no++ ; ?></td>
									<td><?php echo $data->nama_lengkap ; ?></td>
									<td><?php echo $data->no_telp ; ?></td>
									<td><?php echo $data->email ; ?></td>
									<td><?php if ($data->level == 1) {
											echo "Admin";
										} elseif ($data->level == 2) {
											echo "Logistik";
										} elseif ($data->level == 3) {
											echo "Pegawai";
										} else{
											echo "Manajemen";
										}
										?>
									</td>
									<!-- <td><?php echo $data->agama ; ?></td> -->	
									<td class="text-center">
										<form method="post" action="<?php echo base_url('user/del'); ?>">
											<a href="<?php echo base_url('user/edit/'.$data->id_user); ?>" class="btn btn-space btn-primary btn-sm">
												<i class="icon icon-left mdi mdi-edit"></i> Update
											</a>

											<input type="hidden" name="id_user" value="<?php echo $data->id_user ?>">
											<button class="btn btn-space btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin?')">
												<i class="icon icon-left mdi mdi-delete"></i> Delete
											</button>
										</form>
									</td>	
									
								</tr> 
							<?php 
							}
							?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
