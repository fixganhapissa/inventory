<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">User</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url('user'); ?>">Users</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </nav>
    </div>

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                     <div class="panel-heading panel-heading-divider">Tambah User<span class="panel-subtitle"></span></div>
                    <div class="panel-body">

                        <?php //echo validation_errors(); ?>
                        <form action="" method="post">
                            <div class="form-group col-md-6 <?php echo form_error('nama') ? 'has-error' : null ?>">
                                <label >Nama</label>
                                <input class="form-control input-sm" type="text" name="nama" placeholder="Masukkan Nama" value="<?php echo set_value('nama') ?>" >
                               <?php echo form_error('nama') ?>
                            </div>
                            <div class="form-group col-md-6 <?php echo form_error('telepon') ? 'has-error' : null ?>">
                                <label >Telepon</label>
                                <input class="form-control input-sm" type="text" name="telepon" placeholder="Masukkan Telepon" value="<?php echo set_value('telepon') ?>">
                                <?php echo form_error('telepon') ?>
                            </div>

                            <div class="form-group col-md-6 <?php echo form_error('email') ? 'has-error' : null ?>">
                                <label >Email</label>
                                <input class="form-control input-sm" type="email" name="email" placeholder="Masukkan Email" value="<?php echo set_value('email') ?>">
                                <?php echo form_error('email') ?>
                            </div>
                            <div class="form-group col-md-6 <?php echo form_error('username') ? 'has-error' : null ?>">
                                <label >Username</label>
                                <input class="form-control input-sm" type="text" name="username" placeholder="Masukkan Username" value="<?php echo set_value('username') ?>">
                                <?php echo form_error('username') ?>
                            </div>

                            <div class="form-group col-md-6 <?php echo form_error('password') ? 'has-error' : null ?>">
                                <label >Password</label>
                                <input class="form-control input-sm" type="password" name="password" placeholder="Masukkan Password" value="<?php echo set_value('password') ?>" >
                                <?php echo form_error('password') ?>
                            </div>
                            <div class="form-group col-md-6 <?php echo form_error('passconf') ? 'has-error' : null ?>">
                                <label >Password</label>
                                <input class="form-control input-sm" type="password" name="passconf" placeholder="Masukkan Password" value="<?php echo set_value('passconf') ?>" >
                                <?php echo form_error('passconf') ?>
                            </div>

                            <div class="form-group col-md-6 <?php echo form_error('level') ? 'has-error' : null ?>">
                                <label >Level</label>
                                <select class="select2 input-sm" name="level"  <?php echo set_value('level') ?>>
                                    <option selected="selected" value=""> - Pilih - </option>
                                    <option value="1" <?php set_value('level') == 1 ? "selected" : null ?>>Admin</option>
                                    <option value="2" <?php set_value('level') == 2 ? "selected" : null ?>>Logistik</option>
                                    <option value="3" <?php set_value('level') == 3 ? "selected" : null ?>>Pegawai</option>
                                    <option value="4" <?php set_value('level') == 4 ? "selected" : null ?>>Manajemen</option>
                                </select>
                                <?php echo form_error('level') ?>
                            </div>

                            <div class="form-group">
                            <div class="col-sm-4 col-md-12">
                            <p class="text-right">
                                <a href="<?php echo base_url('user'); ?>"><input type="button" class="btn btn-space btn-default" value="Batal"></a>
                                <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                                
                            </p>
                            </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>