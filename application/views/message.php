<?php if($this->session->has_userdata('success')) { ?>
<div role="alert" class="alert alert-success alert-icon alert-dismissible">
	<div class="icon"><span class="mdi mdi-check"></span></div>
	<div class="message">
		<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><?php echo $this->session->flashdata('success'); ?>
	</div>
</div>
<?php } else if($this->session->has_userdata('errors')) { ?>
<div role="alert" class="alert alert-success alert-icon alert-dismissible">
	<div class="icon"><span class="mdi mdi-check"></span></div>
	<div class="message">
		<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><?php echo $this->session->flashdata('errors'); ?>
	</div>
</div>
<?php } else if($this->session->has_userdata('loginfail')) { ?>
<div role="alert" class="alert alert-warning alert-icon alert-dismissible">
	<div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
	<div class="message">
		<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><?php echo $this->session->flashdata('loginfail'); ?>
	</div>
</div>
<?php } ?>
