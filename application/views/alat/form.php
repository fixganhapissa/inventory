<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Alat</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url('alat'); ?>">Alat</a></li>
                <li class="breadcrumb-item active"><?php echo ucfirst($page); ?></li>
            </ol>
        </nav>
    </div>

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                     <div class="panel-heading panel-heading-divider">Form Alat<span class="panel-subtitle"></span></div>
                    <div class="panel-body">

                        <?php //echo validation_errors(); ?>
                        <form action="<?php echo base_url('alat/process'); ?>" method="post">
                            <input type="hidden" name="id" value="<?=$row->id?>">
                        	<!-- <div class="form-group col-md-6">
                                <label >Kode</label>
                                <input class="form-control input-sm" type="text" name="id" value="<?=$row->id?>" required <?php echo $page == 'edit' ? "readonly" : '' ; ?>>
                            </div> -->
                            <div class="form-group col-md-6">
                                <label >Nama Jenis Peralatan</label>
                                <input class="form-control input-sm" type="text" name="nama" value="<?=$row->nama?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >No. Seri Alat</label>
                                <input class="form-control input-sm" type="text" name="no_seri" value="<?=$row->no_seri?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tanggal Kalibrasi</label>
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                    <input class="form-control input-sm" size="16" type="text" name="tanggal_kalibrasi" value="<?=$row->tanggal_kalibrasi?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Program Kalibrasi Ulang</label>
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                    <input class="form-control input-sm" size="16" type="text" name="program_kalibrasi_ulang" value="<?=$row->program_kalibrasi_ulang?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Qty</label>
                                <input class="form-control input-sm" type="number" name="qty" value="<?=$row->qty?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Kategori</label>
                                <select class="select2 input-sm" name="kategori">
                                    <option selected="selected" value=""> - Pilih - </option>
                                    <option value="1" <?php echo $row->kategori == 1 ? 'selected' : "" ; ?>>Alat Kerja</option>
                                    <option value="2" <?php echo $row->kategori == 2 ? 'selected' : "" ; ?>>Alat Pantau</option>
                                    <option value="3" <?php echo $row->kategori == 3 ? 'selected' : "" ; ?>>Alat Ukur</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label ">Keterangan</label>
                                <textarea name="keterangan" rows="4" class="form-control"><?=$row->keterangan?></textarea>
                            </div>

                            <div class="form-group">
	                            <div class="col-sm-4 col-md-12">
	                            <p class="text-right">
	                                <a href="<?php echo base_url('alat'); ?>"><input type="button" class="btn btn-space btn-default" value="Batal"></a>
	                                <button type="submit" name="<?php echo $page ?>" class="btn btn-space btn-primary">Simpan</button> 
	                            </p>
	                            </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>