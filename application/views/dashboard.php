<?php 
	if ($this->session->userdata('level')==1) {
?>
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="email-title"></span> Selamat Datang ! <?php echo $this->fungsi_login->loginuser()->username ?></span>  </div>
		<p class="display-description text-center"></p>
		<div class="row pricing-tables">

			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-face" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('user'); ?>"> Users </a> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	}
?>

<?php 
	if ($this->session->userdata('level')==2) {
?>
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="email-title"></span> Selamat Datang ! <?php echo $this->fungsi_login->loginuser()->username ?></span>  </div>
		<p class="display-description text-center"></p>
		<div class="row pricing-tables">
			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-assignment-check" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('barang'); ?>"> Barang </a> 
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-shape" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('alat'); ?>"> Alat </a> 
					</div>
				</div>
			</div>
			<!-- <div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-roller" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('alat_kerja'); ?>"> Alat Kerja </a> 
					</div>
				</div>
			</div> -->
		<!-- 	<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-flip" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('alat_pantau'); ?>"> Alat Pantau </a> 
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-ruler" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('alat_ukur'); ?>"> Alat Ukur </a> 
					</div>
				</div>
			</div> -->
			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-washing-machine" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('mesin'); ?>"> Mesin </a> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	}
?>

<?php 
	if ($this->session->userdata('level')==3) {
?>
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="email-title"></span> Selamat Datang ! <?php echo $this->fungsi_login->loginuser()->username ?></span>  </div>
		<p class="display-description text-center"></p>
		<div class="row pricing-tables">

			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-square-down" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('barang_masuk'); ?>"> Barang Masuk </a> 
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-square-right" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('barang_keluar'); ?>"> Barang Keluar </a> 
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="pricing-table pricing-table-primary">
					<div class="pricing-table-image">
						<g>
							<center><span class="mdi mdi-redo" style="font-size: 48px; color: DarkSlateGrey;"></span></center>
						</g>
					</div>
					<div class="pricing-table-title" style="color: DarkSlateGrey">
						<a href="<?= base_url('peminjaman_alat'); ?>"> Peminjaman Alat </a> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	}
?>