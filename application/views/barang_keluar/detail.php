<div class="be-content">
    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-success">
                     <div class="panel-heading panel-heading-divider">Detail Barang Keluar<span class="panel-subtitle"></span></div>
                    <div class="panel-body">

                        <form action="" method="post">

                            <div class="col-md-3">
                                <p><strong>Nama Lengkap Peminta</strong></p>
                                <p>No. Telp./HP</p>
                                <p>Proyek</p>
                            </div>
                            <div class="col-md-6">
                                <p><strong><?php echo $data_barang_keluar['barang_keluar']['nama'] ?></strong></p>
                                <p><?php echo $data_barang_keluar['barang_keluar']['no_telp'] ?></p>
                                <p><?php echo $data_barang_keluar['barang_keluar']['proyek'] ?></p>
                            </div>

                            <br /> 
                            <br/>
                            <table class="table table-condensed table-hover table-bordered table-striped" id="alat_info_table">
                                <thead class="text-center">
                                    <tr>
                                        <th style="width:5%">No</th>
                                        <th style="width:30%">Barang</th>
                                        <th style="width:10%">Type</th>
                                        <th style="width:15%">Vendor</th>
                                        <th style="width:10%">Qty</th>
                                        <th style="width:30%">Keterangan</th>
                                    </tr>
                                </thead>

                                <tbody class="no-border-x">
                                <?php if(isset($data_barang_keluar['detail_barang_keluar'])): ?>
                                    <?php $x = 1; ?>
                                    <?php foreach ($data_barang_keluar['detail_barang_keluar'] as $key => $val): ?>
                                    <tr id="row_<?php echo $x; ?>">
                                        <td><?php echo $x++ ; ?></td>
                                        <td><?php echo $val['nama_barang'] ?></td>
                                        <td><?php echo $val['type'] ?></td>
                                        <td><?php echo $val['vendor'] ?></td>
                                        <td><?php echo $val['qty'] ?></td>
                                        <td><?php echo $val['keterangan'] ?></td>
                                    </tr>
                                    <?php $x++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>

                            <?php if ($this->session->userdata('level')==2) { ?>
                            <?php $status = $data_barang_keluar['barang_keluar']['status'] ; ?>
                            <div class="form-group col-sm-4 col-sm-offset-8">
                                <label class="col-sm-3 control-label"><strong>Status Persetujuan</strong></label>
                                <div class="col-sm-9">
                                <select class="select2 form-control-sm" name="status" required>
                                        <option value="1">Setujui </option>
                                        <option value="2" <?php echo $status == 2 ? 'selected' : "" ; ?>>Tolak </option>
                                </select>
                                </div>
                            </div>
                            <?php
                            }
                            ?>

                            <div class="form-group">
                                <div class="col-sm-4 col-md-12">
                                <p class="text-right">
                                    <a href="<?php echo base_url('barang_keluar'); ?>"><input type="button" class="btn btn-space btn-default" value="Batal"></a>
                                    <button type="submit" class="btn btn-space btn-primary">Simpan</button> 
                                </p>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

