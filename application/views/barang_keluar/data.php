<div class="be-content">
    <!-- <div class="page-head">
        <h2 class="page-head-title">Barang Keluar</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url('barang'); ?>">Barang Keluar</a></li>
                <li class="breadcrumb-item active">Data Barang Keluar</li>
            </ol>
        </nav>
    </div> -->

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div id="messages" ></div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-danger">
                    <div class="panel-heading">Data Barang Keluar
                        <div class="tools"><a href="<?php echo base_url('barang_keluar/create'); ?>"><button class="btn btn-primary btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-plus"></i> </span><span class="btn-text">Tambah Data</span></button></a></div>
                    </div>
                    <div class="panel-body">
                        <!-- <table id="manageTable" class="table table-bordered table-striped"> -->
                        <table class="table table-striped table-hover table-fw-widget" id="manageTable">
                            <thead>
                                <tr>
                                    <!-- <th>No</th> -->
                                    <th>Kode</th>
                                    <th>Nama Lengkap Peminta</th>
                                    <th>No Telepon</th>
                                    <th>Proyek</th>
                                    <th>Tanggal Permintaan</th>
                                    <th>Tanggal Pengeluaran</th>
                                    <th>Status</th>
                                    <th style="width:5%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                           
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Remove Order</h4>
            </div>

            <form role="form" action="<?php echo base_url('barang_keluar/remove') ?>" method="post" id="removeForm">
            <div class="modal-body">
                <p>Do you really want to remove?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
var manageTable;
var base_url = "<?php echo base_url(); ?>";

$(document).ready(function() {

    // initialize the datatable 
    manageTable = $('#manageTable').dataTable({
        'ajax': base_url + 'barang_keluar/fetchBarangKeluarData',
        'barang_keluar': []
    });
});


// remove functions 
function removeFunc(id)
{
    if(id) {
    $("#removeForm").on('submit', function() {

        var form = $(this);

        // remove the text-danger
        $(".text-danger").remove();
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: { id_barang_keluar:id }, 
            dataType: 'json',
            success:function(response) {
                var table = $('#manageTable').DataTable();
                table.ajax.reload(null, false); 

            if(response.success === true) {
                $("#messages").html('<div class="alert alert-primary alert-icon alert-dismissible" role="alert">'+
                    '<div class="icon"><span class="mdi mdi-check"></span></div>'+
                        '<div class="message">'+
                            '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>'+response.messages+
                        '</div>'+
                    '</div>');
                // hide modal
                $("#removeModal").modal('hide');
            } else {
                $("#messages").html('<div class="alert alert-primary alert-icon alert-dismissible" role="alert">'+
                    '<div class="icon"><span class="mdi mdi-check"></span></div>'+
                        '<div class="message">'+
                            '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>'+response.messages+
                        '</div>'+
                    '</div>'); 
                }
            }
        }); 
        return false;
    });
    }
}
</script>

