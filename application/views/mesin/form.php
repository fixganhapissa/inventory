<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Mesin</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url('mesin'); ?>">Mesin</a></li>
                <li class="breadcrumb-item active"><?php echo ucfirst($page); ?></li>
            </ol>
        </nav>
    </div>

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                     <div class="panel-heading panel-heading-divider">Form Mesin<span class="panel-subtitle"></span></div>
                    <div class="panel-body">

                        <form action="<?php echo base_url('mesin/process'); ?>" method="post">
                            <input type="hidden" name="id" value="<?=$row->id?>">
                           <!--  <div class="form-group col-md-6">
                                <label >Kode Mesin</label>
                                <input class="form-control input-sm" type="text" name="id" value="<?=$row->id?>" required <?php echo $page == 'edit' ? "readonly" : '' ; ?>>
                            </div> -->
                            <div class="form-group col-md-6">
                                <label >Nama Mesin</label>
                                <input class="form-control input-sm" type="text" name="nama" value="<?=$row->nama?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Model</label>
                                <input class="form-control input-sm" type="text" name="model" value="<?=$row->model?>" required>
                            </div>
                             <div class="form-group col-md-6">
                                <label >Kondisi</label>
                                <input class="form-control input-sm" type="text" name="kondisi" value="<?=$row->kondisi?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Lokasi</label>
                                <input class="form-control input-sm" type="text" name="lokasi" value="<?=$row->lokasi?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Jumlah</label>
                                <input class="form-control input-sm" type="number" name="jumlah" value="<?=$row->jumlah?>" required>
                            </div>
                            <div class="form-group col-md-6">
			                    <label ">Spesifikasi</label>
			                    <textarea name="spesifikasi" rows="4" class="form-control"><?=$row->spesifikasi?></textarea>
		                    </div>
                            <div class="form-group col-md-6"> 
                            </div>
                            
                            <div class="form-group">
	                            <div class="col-sm-4 col-md-12">
	                            <p class="text-right">
	                                <a href="<?php echo base_url('mesin'); ?>"><input type="button" class="btn btn-space btn-default" value="Batal"></a>
	                                <button type="submit" name="<?php echo $page ?>" class="btn btn-space btn-primary">Simpan</button> 
	                            </p>
	                            </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>