<div class="be-content">

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-danger">
                     <div class="panel-heading panel-heading-divider">Form Permintaan Material<span class="panel-subtitle"> <?php echo date('Y-m-d') ?></span></div>
                    <div class="panel-body">

                        <form action="" method="post">

                            <div class="form-group col-md-4 <?php echo form_error('nama') ? 'has-error' : null ?>">
                                <label >Nama Lengkap Peminta</label>
                                <input class="form-control input-sm" type="text" id="nama" name="nama" placeholder="Masukkan Nama Lengkap Peminta" value="<?php echo $data_permintaan['permintaan']['nama'] ?>" >
                               <?php echo form_error('nama') ?>
                            </div>
                            <div class="form-group col-md-4 <?php echo form_error('no_telp') ? 'has-error' : null ?>">
                                <label >No. Telp./ HP</label>
                                <input class="form-control input-sm" type="text" name="no_telp" placeholder="Masukkan No. Telp./HP" value="<?php echo $data_permintaan['permintaan']['no_telp'] ?>" >
                               <?php echo form_error('no_telp') ?>
                            </div>
                            <div class="form-group col-md-4 <?php echo form_error('proyek') ? 'has-error' : null ?>">
                                <label >Proyek</label>
                                <input class="form-control input-sm" type="text" name="proyek" placeholder="Masukkan Proyek" value="<?php echo $data_permintaan['permintaan']['proyek'] ?>" >
                               <?php echo form_error('proyek') ?>
                            </div>
                            <div class="form-group col-md-4 <?php echo form_error('tanggal_permintaan') ? 'has-error' : null ?>">
                                <label >Tanggal Permintaan</label>
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                    <input class="form-control input-sm" size="16" type="text" name="tanggal_permintaan" value="<?php echo $data_permintaan['permintaan']['tanggal_permintaan'] ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-4 <?php echo form_error('tanggal_penyerahan') ? 'has-error' : null ?>">
                                <label >Tanggal Penyerahan</label>
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                    <input class="form-control input-sm" size="16" type="text" name="tanggal_penyerahan" value="<?php echo $data_permintaan['permintaan']['tanggal_penyerahan'] ?>" >
                                </div>
                            </div>

                            <br /> <br/>
                            <table class="table " id="material_info_table">
                                <thead>
                                    <tr>
                                        <th style="width:25%">Material</th>
                                        <th style="width:10%">Type</th>
                                        <th style="width:15%">Vendor</th>
                                        <th style="width:10%">Jumlah</th>
                                        <th style="width:30%">Keterangan</th>
                                        <th style="width:10%" align="text-center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="add_row" class="btn btn-default"><i class="icon icon-left mdi mdi-plus"></i></button></th>
                                    </tr>
                                </thead>

                                <tbody>
                                <?php if(isset($data_permintaan['detail_permintaan'])): ?>
                                    <?php $x = 1; ?>
                                    <?php foreach ($data_permintaan['detail_permintaan'] as $key => $val): ?>
                                        <?php //print_r($v); ?>
                                    <tr id="row_<?php echo $x; ?>">
                                    <!-- <tr id="row_1"> -->
                                        <td>
                                            <select class="select2 form-control-sm " data-row-id="row_<?php echo $x; ?>" id="permintaan_<?php echo $x; ?>" name="material[]" style="width:100%;" onchange="getBarangData(<?php echo $x; ?>)" required>
                                                <option value="">-- Pilih Material --</option>
                                                <?php foreach ($material as $k => $v): ?>
                                                    <option value="<?php echo $v['id'] ?>" 
                                                        <?php if($val['id_material'] == $v['id']) { 
                                                            echo "selected='selected'"; } ?>>
                                                            <?php echo $v['nama'] ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input class="form-control input-sm" type="text" name="type[]" id="type_<?php echo $x; ?>" value="<?php echo $val['type'] ?>" autocomplete="off">
                                        </td>
                                        <td>
                                            <input class="form-control input-sm" type="text" name="vendor[]" id="vendor_<?php echo $x; ?>" value="<?php echo $val['vendor'] ?>" autocomplete="off">
                                        </td>
                                        <td>
                                            <input class="form-control input-sm" type="number" name="qty[]" id="qty_<?php echo $x; ?>" value="<?php echo $val['qty'] ?>" required>
                                        </td>
                                        <td>
                                            <input class="form-control input-sm" type="text" name="keterangan[]" id="keterangan_<?php echo $x; ?>" value="<?php echo $val['keterangan'] ?>">
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-default" onclick="removeRow('1')"><i class="icon icon-left mdi mdi-delete"></i></button></td>
                                    </tr>
                                    <?php $x++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>

                            <div class="form-group">
                                <div class="col-sm-4 col-md-12">
                                <p class="text-right">
                                    <a href="<?php echo base_url('permintaan'); ?>"><input type="button" class="btn btn-space btn-default" value="Batal"></a>
                                    <button type="submit" class="btn btn-space btn-primary">Simpan</button> 
                                </p>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var base_url = "<?php echo base_url(); ?>";
    $(document).ready(function() {
    $(".select_group").select2();
  
    // Add row baru pada table 
    $("#add_row").unbind('click').bind('click', function() {
        var table = $("#material_info_table");
        var count_table_tbody_tr = $("#material_info_table tbody tr").length;
        var row_id = count_table_tbody_tr + 1;

        $.ajax({
            url: base_url + '/permintaan/getTableMaterialRow/',
            type: 'post',
            dataType: 'json',
            success:function(response) {
              // console.log(reponse.x);
                var html = '<tr id="row_'+row_id+'">'+
                    '<td>'+ 
                    '<select class="form-control input-sm select_group material" data-row-id="'+row_id+'" id="material_'+row_id+'" name="material[]" style="width:100%;" onchange="getMaterialData('+row_id+')">'+
                        '<option value="">-- Pilih Material --</option>';
                        $.each(response, function(index, value) {
                            html += '<option value="'+value.id+'">'+value.nama+'</option>';             
                        });
                        
                        html += '</select>'+
                    '</td>'+ 
                    '<td><input class="form-control input-sm" type="text" name="type[]" id="type_'+row_id+'"></td>'+
                    '<td><input class="form-control input-sm" type="text" name="vendor[]" id="vendor_'+row_id+'"></td>'+
                    '<td><input class="form-control input-sm" type="number" name="qty[]" id="qty_'+row_id+'" onkeyup="getTotal('+row_id+')"></td>'+
                    '<td><input class="form-control input-sm" type="text" name="keterangan[]" placeholder="keterangan" id="keterangan_'+row_id+'"></td>'+
                    '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-default" onclick="removeRow(\''+row_id+'\')"><i class="icon icon-left mdi mdi-delete"></i></button></td>'+
                    '</tr>';

                if(count_table_tbody_tr >= 1) {
                $("#material_info_table tbody tr:last").after(html);  
                }
                else {
                    $("#material_info_table tbody").html(html);
                }

                $(".material").select2();
            }
        });

        return false;
    });

    }); // /document


    // get material dari sever
    function getMaterialData(row_id)
    {
        var material_id = $("#material_"+row_id).val();    
        if(material_id == "") {

            $("#type_"+row_id).val("");
            $("#vendor_"+row_id).val("");

            $("#qty_"+row_id).val("");        
            $("#keterangan_"+row_id).val("");        
        } else {
            $.ajax({
                url: base_url + 'permintaan/getMaterialValueById',
                type: 'post',
                data: {material_id : material_id},
                dataType: 'json',
                success:function(response) {

                    // setting value ke input field
                    $("#type_"+row_id).val("");
                    $("#type_value_"+row_id).val("");
                    $("#vendor_"+row_id).val("");
                    $("#vendor_value_"+row_id).val("");

                    $("#qty_"+row_id).val(1);
                    $("#qty_value_"+row_id).val(1);
                    $("#keterangan_"+row_id).val("");
                    $("#keterangan_value_"+row_id).val("");
                } // /success
            }); // ajax fungsi untuk mengambil data material
        }
    }

    function removeRow(tr_id)
    {
        $("#material_info_table tbody tr#row_"+tr_id).remove();
    }
</script>
