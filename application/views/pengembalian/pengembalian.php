<div class="be-content">
    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-success">
                     <div class="panel-heading panel-heading-divider">Form Peminjaman Alat<span class="panel-subtitle"> <?php echo date('Y-m-d') ?></span></div>
                    <div class="panel-body">

                        <form action="" method="post">

                            <div class="col-md-3">
                                <p><strong>Nama Lengkap Peminjam</strong></p>
                                <p>No. Telp./HP</p>
                                <p>Proyek</p>
                                <p>Tanggal Peminjaman</p>
                                <p>Tanggal Pengembalian</p>
                            </div>
                            <div class="col-md-6">
                                <p><strong><?php echo $data_peminjaman['peminjaman']['nama'] ?></strong></p>
                                <p><?php echo $data_peminjaman['peminjaman']['no_telp'] ?></p>
                                <p><?php echo $data_peminjaman['peminjaman']['proyek'] ?></p>
                                <p><?php echo longdate_indo($data_peminjaman['peminjaman']['tanggal_peminjaman']) ?></p>
                                <p><?php echo longdate_indo($data_peminjaman['peminjaman']['tanggal_pengembalian']) ?></p>
                            </div>

                            <div class="col-md-1">
                                <p><strong>Status</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $data_peminjaman['peminjaman']['status'] == 1 ? '<span class="label label-warning">Dipinjam</span>' : '<span class="label label-success">Dikembalikan</span>' ; ?></p>
                            </div>

                            <br /> 
                            <br/>
                            <table class="table table-condensed table-hover table-bordered table-striped" id="alat_info_table">
                                <thead class="text-center">
                                    <tr>
                                        <th style="width:5%">No</th>
                                        <th style="width:35%">Alat yang dipinjam</th>
                                        <th style="width:10%">Jumlah</th>
                                        <th style="width:20%">Status</th>
                                        <th style="width:30%">Keterangan</th>
                                    </tr>
                                </thead>

                                <tbody class="no-border-x">
                                <?php if(isset($data_peminjaman['detail_peminjaman'])): ?>
                                    <?php $x = 1; ?>
                                    <?php foreach ($data_peminjaman['detail_peminjaman'] as $key => $val): ?>
                                    <tr id="row_<?php echo $x; ?>">
                                        <td><?php echo $x++ ; ?></td>
                                        <td><?php echo $val['nama_alat'] ?></td>
                                        <td>
                                            <?php echo $val['qty'] ?>
                                            <input type="hidden" name="qty[]" id="qty_<?php echo $x; ?>" value="<?php echo $val['qty'] ?>" >    
                                        </td>
                                        <td><?php echo $val['status'] ?></td>
                                        <td><?php echo $val['keterangan'] ?></td>
                                    </tr>
                                    <?php $x++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>


                            
                            <input class="form-control input-sm" type="hidden" name="status" value="2" >

                            <!-- <?php $status = $data_peminjaman['peminjaman']['status'] ; ?>
                            <div class="form-group col-sm-4 col-sm-offset-8" <?php echo form_error('nama') ? 'has-error' : null ?>">
                                <label class="col-sm-3 control-label"><strong>Status</strong></label>
                                <div class="col-sm-9">
                                <select class="select2 form-control-sm" name="status" required>
                                        <option value="1">Dipinjam </option>
                                        <option value="2" <?php echo $status == 2 ? 'selected' : "" ; ?>>Dikembalikan </option>
                                </select>
                                </div>
                            </div> -->

                            <?php 
                                $status = $data_peminjaman['peminjaman']['status'] ;
                                if ($status == 1) { ?>
                                <div class="row invoice-footer">
                                    <div class="col-md-12">
                                        <a href="<?php echo base_url('peminjaman'); ?>"><input type="button" class="btn btn-lg btn-space btn-default" value="Batal"></a>
                                        <button type="submit"  class="btn btn-lg btn-space btn-primary">Kembalikan Peminjaman</button>
                                    </div>
                                </div>
                            <?php
                                } else
                                {
                                    echo '';
                                }
                            ?>
                           

                            <!-- <div class="form-group">
                                <div class="col-sm-4 col-md-12">
                                <p class="text-right">
                                    <a href="<?php echo base_url('peminjaman'); ?>"><input type="button" class="btn btn-space btn-default" value="Batal"></a>
                                    <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                                </p>
                                </div>
                            </div> -->
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var base_url = "<?php echo base_url(); ?>";
    $(document).ready(function() {
    $(".select_group").select2();
  
    // Add row baru pada table 
    $("#add_row").unbind('click').bind('click', function() {
        var table = $("#alat_info_table");
        var count_table_tbody_tr = $("#alat_info_table tbody tr").length;
        var row_id = count_table_tbody_tr + 1;

        $.ajax({
            url: base_url + '/peminjaman/getTableAlatRow/',
            type: 'post',
            dataType: 'json',
            success:function(response) {
              // console.log(reponse.x);
                var html = '<tr id="row_'+row_id+'">'+
                    '<td>'+ 
                    '<select class="form-control input-sm select_group alat" data-row-id="'+row_id+'" id="alat_'+row_id+'" name="alat[]" style="width:100%;" onchange="getAlatData('+row_id+')">'+
                        '<option value="">-- Pilih Alat --</option>';
                        $.each(response, function(index, value) {
                            html += '<option value="'+value.id+'">'+value.nama+'</option>';             
                        });
                        
                        html += '</select>'+
                    '</td>'+ 
                    '<td><input class="form-control input-sm" type="number" name="qty[]" id="qty_'+row_id+'" onkeyup="getTotal('+row_id+')"></td>'+
                    '<td><input class="form-control input-sm" type="number" name="status[]" id="status_'+row_id+'" placeholder="status" onkeyup="getTotal('+row_id+')"></td>'+
                    '<td><input class="form-control input-sm" type="text" name="keterangan[]" placeholder="keterangan" id="keterangan_'+row_id+'"></td>'+
                    '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-default" onclick="removeRow(\''+row_id+'\')"><i class="icon icon-left mdi mdi-delete"></i></button></td>'+
                    '</tr>';

                if(count_table_tbody_tr >= 1) {
                $("#alat_info_table tbody tr:last").after(html);  
                }
                else {
                    $("#alat_info_table tbody").html(html);
                }

                $(".alat").select2();
            }
        });

        return false;
    });

    }); // /document


    // get alat dari sever
        function getAlatData(row_id)
    {
        var alat_id = $("#alat_"+row_id).val();    
        if(alat_id == "") {
            $("#qty_"+row_id).val("");        
            $("#status_"+row_id).val("");        
            $("#keterangan_"+row_id).val("");        
        } else {
            $.ajax({
                url: base_url + 'peminjaman/getAlatValueById',
                type: 'post',
                data: {alat_id : alat_id},
                dataType: 'json',
                success:function(response) {

                    // setting value ke input field
                    $("#qty_"+row_id).val(1);
                    $("#qty_value_"+row_id).val(1);
                    $("#status_"+row_id).val("");
                    $("#status_value_"+row_id).val("");
                    $("#keterangan_"+row_id).val("");
                    $("#keterangan_value_"+row_id).val("");
                } // /success
            }); // ajax fungsi untuk mengambil data alat
        }
    }

    function removeRow(tr_id)
    {
        $("#alat_info_table tbody tr#row_"+tr_id).remove();
    }
</script>
