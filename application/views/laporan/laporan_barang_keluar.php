<div class="be-content">
    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div id="messages" ></div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-danger">
                    <div class="panel-heading">Laporan Barang Keluar

                    </div>

                    <div class="form-group col-md-4">
                        <label >Tanggal Permintaan</label>
                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            <input class="form-control input-sm" size="16" type="text" name="tanggal_permintaan" value="">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label >Tanggal Pengeluaran</label>
                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            <input class="form-control input-sm" size="16" type="text" name="tanggal_pengeluaran" value="">
                        </div>
                    </div>
                    <div class=" col-md-2">
                        <label></label>
                        <br>
                        <br>
                        <div>
                        <button type="button" class="btn btn-space btn-primary">Filter</button> 
                        </div>
                    </div>


                    <div class="panel-body">
                        <!-- <table id="manageTable" class="table table-bordered table-striped"> -->
                        <table class="table table-striped table-hover table-fw-widget" id="manageTable">
                            <thead>
                                <tr>
                                    <!-- <th>No</th> -->
                                    <th>Kode</th>
                                    <th>Nama Lengkap Peminta</th>
                                    <th>No Telepon</th>
                                    <th>Proyek</th>
                                    <th>Tanggal Permintaan</th>
                                    <th>Tanggal Pengeluaran</th>
                                    <th>Status</th>
                                    <th style="width:5%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                           
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var manageTable;
var base_url = "<?php echo base_url(); ?>";

$(document).ready(function() {

    // initialize the datatable 
    manageTable = $('#manageTable').dataTable({
        'ajax': base_url + 'laporan_barang_keluar/fetchBarangKeluarData',
        'laporan_barang_keluar': []
    });
});


// remove functions 
function removeFunc(id)
{
    if(id) {
    $("#removeForm").on('submit', function() {

        var form = $(this);

        // remove the text-danger
        $(".text-danger").remove();
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: { id_barang_keluar:id }, 
            dataType: 'json',
            success:function(response) {
                var table = $('#manageTable').DataTable();
                table.ajax.reload(null, false); 

            if(response.success === true) {
                $("#messages").html('<div class="alert alert-primary alert-icon alert-dismissible" role="alert">'+
                    '<div class="icon"><span class="mdi mdi-check"></span></div>'+
                        '<div class="message">'+
                            '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>'+response.messages+
                        '</div>'+
                    '</div>');
                // hide modal
                $("#removeModal").modal('hide');
            } else {
                $("#messages").html('<div class="alert alert-primary alert-icon alert-dismissible" role="alert">'+
                    '<div class="icon"><span class="mdi mdi-check"></span></div>'+
                        '<div class="message">'+
                            '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>'+response.messages+
                        '</div>'+
                    '</div>'); 
                }
            }
        }); 
        return false;
    });
    }
}
</script>

