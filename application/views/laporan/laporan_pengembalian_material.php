<div class="be-content">
    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div id="messages" ></div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-danger">
                    <div class="panel-heading">Laporan Pengembalian Material
                    </div>
                    <div class="panel-body">
                        <!-- <table id="manageTable" class="table table-bordered table-striped"> -->
                        <table class="table table-striped table-hover table-fw-widget" id="manageTable">
                            <thead>
                                <tr>
                                    <!-- <th>No</th> -->
                                    <th>Nama Lengkap Pengirim</th>
                                    <th>No Telepon</th>
                                    <th>Proyek</th>
                                    <th>Tanggal Pengembalian</th>
                                    <th class="text-center" style="width:12%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                           
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var manageTable;
var base_url = "<?php echo base_url(); ?>";

$(document).ready(function() {

    // initialize the datatable 
    manageTable = $('#manageTable').dataTable({
        'ajax': base_url + 'laporan_pengembalian_material/fetchPengembalianData',
        'laporan_pengembalian_material': []
    });
});

</script>

