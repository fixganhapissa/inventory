<div class="be-content">
    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div id="messages" ></div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-success">
                    <div class="panel-heading">Laporan Data Peminjaman
                    </div>

                    <div class="form-group col-md-4">
                        <label >Tanggal Peminjaman</label>
                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            <input class="form-control input-sm" size="16" type="text" name="tanggal_peminjaman" value="">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label >Tanggal Pengembalian</label>
                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            <input class="form-control input-sm" size="16" type="text" name="tanggal_pengembalian" value="">
                        </div>
                    </div>
                    <div class=" col-md-2">
                        <label></label>
                        <br>
                        <br>
                        <div>
                        <button type="button" class="btn btn-space btn-primary">Filter</button> 
                        </div>
                    </div>

                    <div class="panel-body">
                        <!-- <table id="manageTable" class="table table-bordered table-striped"> -->
                        <table class="table table-striped table-hover table-fw-widget" id="manageTable">
                            <thead>
                                <tr>
                                    <!-- <th>No</th> -->
                                    <th>Nama Lengkap Pengirim</th>
                                    <th>No Telepon</th>
                                    <th>Proyek</th>
                                    <th>Tanggal Peminjaman</th>
                                    <th>Tanggal Pengembalian</th>
                                    <th>Status</th>
                                    <th style="width:5%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                           
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var manageTable;
var base_url = "<?php echo base_url(); ?>";

$(document).ready(function() {

    // initialize the datatable 
    manageTable = $('#manageTable').dataTable({
        'ajax': base_url + 'laporan_peminjaman_alat/fetchPeminjamanData',
        'laporan_peminjaman_alat': []
    });
});

</script>

