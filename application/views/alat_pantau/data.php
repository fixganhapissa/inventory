<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Alat Pantau</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url('alat_pantau'); ?>">Alat Pantau</a></li>
                <li class="breadcrumb-item active">Alat Pantau</li>
            </ol>
        </nav>
    </div>

    <div class="main-content container-fluid">
        <!--Basic Elements-->
        <?php $this->view('message') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                    <div class="panel-heading">Data Alat Pantau
                        <div class="tools"><a href="<?php echo base_url('alat_pantau/add'); ?>"><button class="btn btn-primary btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-plus"></i> </span><span class="btn-text">Tambah Data</span></button></a></div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-fw-widget" id="table1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Jenis Peralatan</th>
                                    <th>No. Seri Alat</th>
                                    <th>Tanggal Kalibrasi</th>
                                    <th>Program Kalibrasi Ulang</th>
                                    <th>Qty</th>
                                    <th>Keterangan</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $no = 1;
                                foreach ($row->result() as $key => $data) {
                                # code...
                            ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $no++ ; ?></td>
                                    <td><?php echo $data->nama ; ?></td>
                                    <td><?php echo $data->no_seri ; ?></td>
                                    <td><?php echo $data->tanggal_kalibrasi ; ?></td>
                                    <td><?php echo $data->program_kalibrasi_ulang ; ?></td> 
                                    <td><?php echo $data->qty ; ?></td>
                                    <td><?php echo $data->keterangan ; ?></td>
                                    <td class="text-center">

                                        <a href="<?php echo base_url('alat_pantau/edit/'.$data->id); ?>">
                                            <button class="btn btn-space btn-primary btn-sm"><i class="icon icon-left mdi mdi-edit"></i> Edit</button>
                                        </a>

                                        <a href="<?php echo base_url('alat_pantau/del/'.$data->id); ?>" onclick="return confirm('Apakah Anda Yakin?')">
                                            <button class="btn btn-space btn-danger btn-sm"><i class="icon icon-left mdi mdi-delete"></i> Delete</button>
                                        </a>

                                       
                                    </td>   
                                    
                                </tr> 
                            <?php 
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
