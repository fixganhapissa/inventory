<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/tmu-favicon.ico">
    <title>Inventory - PT Techno Multi Utama</title>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/datatables/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/bootstrap-slider/css/bootstrap-slider.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>

    <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.dataTables();
            App.formElements();
            // App.uiNotifications();
        });
    </script>
</head>
<body>
    <div class="be-wrapper be-fixed-sidebar">
        <nav class="navbar navbar-default navbar-fixed-top be-top-header">
            <div class="container-fluid">
                <div class="navbar-header"><a href="<?php echo base_url('dashboard'); ?>" class="navbar-brand"></a></div>
                <div class="be-right-navbar">
                    <ul class="nav navbar-nav navbar-right be-user-nav">
                        <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="<?php echo base_url(); ?>assets/img/avatar.png" alt="Avatar"><span class="user-name"><?php echo $this->fungsi_login->loginuser()->username ?></span></a>
                            <ul role="menu" class="dropdown-menu">

                                <li>
                                    <div class="user-info">
                                        <div class="user-name"><?php echo $this->fungsi_login->loginuser()->nama_lengkap ?> </div>
                                        <div class="user-position online">Available</div>
                                    </div>
                                </li>
                                
                                <li><a href="<?php echo base_url('auth/logout'); ?>"><span class="icon mdi mdi-power"></span> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="page-title"><span>Inventory - PT Techno Multi Utama</span></div> 
                </div>
            </div>
        </nav>

        <div class="be-left-sidebar">
            <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
                <div class="left-sidebar-spacer">
                    <div class="left-sidebar-scroll">
                        <div class="left-sidebar-content">
                            <ul class="sidebar-elements">
                                <li class="divider">Menu</li>
                                <li <?php echo $this->uri->segment(1) == 'dashboard' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>>
                                    <a href="<?php echo base_url('dashboard') ?>">
                                    <i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                                </li>


                                <?php 
                                if ($this->session->userdata('level')==1) {
                                ?>
                                    <li <?php echo $this->uri->segment(1) == 'user' ? 'class="active"' : '' ?>>
                                        <a href="<?php echo base_url('user') ?>"><i class="icon mdi mdi-face"></i><span>Users</span></a>
                                    </li>
                                <?php 
                                }
                                ?>

                                <?php 
                                // if ($this->fungsi->loginuser()->level == 2) {
                                if ($this->session->userdata('level')==2) {
                                ?>  
                                    <li class="divider">Alat & Barang</li>
                                    <li class="parent"><a href="#"><i class="icon mdi mdi-assignment-check"></i><span>Barang</span></a>
                                        <ul class="sub-menu">
                                            <li <?php echo $this->uri->segment(1) == 'barang' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('barang') ?>">Data Barang</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'barang_masuk' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('barang_masuk') ?>">Barang Masuk</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'barang_keluar' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('barang_keluar') ?>">Barang Keluar</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="parent"><a href="#"><i class="icon mdi mdi-shape"></i><span>Alat</span></a>
                                        <ul class="sub-menu">
                                            <li <?php echo $this->uri->segment(1) == 'alat' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('alat') ?>">Alat</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'peminjaman' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('peminjaman') ?>">Peminjaman</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li <?php echo $this->uri->segment(1) == 'mesin' ? 'class="active"' : '' ?>>
                                        <a href="<?php echo base_url('mesin') ?>"><i class="icon mdi mdi-washing-machine"></i><span>Mesin</span></a>
                                    </li>

                                    <li class="divider">Stock & Laporan</li>
                                    <li class="parent"><a href="#"><i class="icon mdi mdi-markunread-mailbox"></i><span>Kartu Stok</span></a>
                                        <ul class="sub-menu">
                                            <li <?php echo $this->uri->segment(1) == 'stok_barang' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('stok_barang') ?>">Barang</a>
                                            </li>
                                           <!--  <li <?php echo $this->uri->segment(1) == 'stok_material' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('stok_material') ?>">Material</a>
                                            </li> -->
                                        </ul>
                                    </li>

                                    <li class="parent"><a href="#"><i class="icon mdi mdi-view-list"></i><span>Laporan</span></a>
                                        <ul class="sub-menu">
                                            <li <?php echo $this->uri->segment(1) == 'laporan_barang_masuk' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_barang_masuk') ?>">Barang Masuk</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'laporan_barang_keluar' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_barang_keluar') ?>">Barang Keluar</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'laporan_peminjaman_alat' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_peminjaman_alat') ?>">Peminjaman Alat</a>
                                            </li>
                                           <!--  <li <?php echo $this->uri->segment(1) == 'laporan_permintaan_material' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_permintaan_material') ?>">Permintaan Material</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'laporan_pengembalian_material' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_pengembalian_material') ?>">Pengembalian Material</a>
                                            </li> -->
                                        </ul>
                                    </li>
                                <?php 
                                }
                                ?>
                                <?php 
                                if ($this->session->userdata('level')==3){
                                ?>  
                                    <li <?php echo $this->uri->segment(1) == 'barang_masuk' ? 'class="active"' : '' ?>>
                                        <a href="<?php echo base_url('barang_masuk') ?>"><i class="icon mdi mdi-square-down"></i><span>Barang Masuk</span></a>
                                    </li>
                                    <li <?php echo $this->uri->segment(1) == 'barang_keluar' ? 'class="active"' : '' ?>>
                                        <a href="<?php echo base_url('barang_keluar') ?>"><i class="icon mdi mdi-square-right"></i><span>Barang Keluar</span></a>
                                    </li>

                                    <li <?php echo $this->uri->segment(1) == 'peminjaman' ? 'class="active"' : '' ?>>
                                        <a href="<?php echo base_url('peminjaman') ?>"><i class="icon mdi mdi-redo"></i><span>Peminjaman Alat</span></a>
                                    </li>

                                <?php 
                                }
                                ?>
                                 <?php 
                                if ($this->session->userdata('level')==4){
                                ?>  
                                   <li class="parent"><a href="#"><i class="icon mdi mdi-markunread-mailbox"></i><span>Kartu Stok</span></a>
                                        <ul class="sub-menu">
                                            <li <?php echo $this->uri->segment(1) == 'stok_barang' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('stok_barang') ?>">Barang</a>
                                            </li>
                                           <!--  <li <?php echo $this->uri->segment(1) == 'stok_material' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('stok_material') ?>">Material</a>
                                            </li> -->
                                        </ul>
                                    </li>
                                     <li class="parent"><a href="#"><i class="icon mdi mdi-view-list"></i><span>Laporan</span></a>
                                        <ul class="sub-menu">
                                            <li <?php echo $this->uri->segment(1) == 'laporan_barang_masuk' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_barang_masuk') ?>">Barang Masuk</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'laporan_barang_keluar' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_barang_keluar') ?>">Barang Keluar</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'laporan_peminjaman_alat' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_peminjaman_alat') ?>">Peminjaman Alat</a>
                                            </li>
                                           <!--  <li <?php echo $this->uri->segment(1) == 'laporan_permintaan_material' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_permintaan_material') ?>">Permintaan Material</a>
                                            </li>
                                            <li <?php echo $this->uri->segment(1) == 'laporan_pengembalian_material' ? 'class="active"' : '' ?>>
                                                <a href="<?php echo base_url('laporan_pengembalian_material') ?>">Pengembalian Material</a>
                                            </li> -->
                                        </ul>
                                    </li>
                                <?php 
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $contents; ?>

    </div>

    <script src="<?php echo base_url(); ?>assets/js/main.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="<?php echo base_url(); ?>assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/parsley/parsley.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/app-form-elements.js" type="text/javascript"></script>

    <script type="text/javascript">
        $('#example').dataTables({
            "language": {
            "emptyTable": "asdsa"
            }
        });
    </script>
</body>
</html>