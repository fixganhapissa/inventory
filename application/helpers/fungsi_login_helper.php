<?php
	
	function sudahlogin(){
		$ci = & get_instance();
		$sesiuser = $ci->session->userdata('id_user');
		if($sesiuser){
			redirect('dashboard');
		}
	}

	function belumlogin(){
		$ci = & get_instance();
		$sesiuser = $ci->session->userdata('id_user');
		if(!$sesiuser){
			redirect('auth/login');
		}
	}

	function hak_akses(){
		$ci = & get_instance();
		$ci->load->library('fungsi_login');
		if ($ci->fungsi_login->loginuser()->level != 1) {
			redirect('dashboard');
		}
	}
	

?>