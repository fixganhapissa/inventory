<?php

Class Fungsi_login{
	
	Protected $ci;

	function __construct(){
		$this->ci = & get_instance();
	}

	function loginuser(){
		$this->ci->load->model('M_login');
		$id_user = $this->ci->session->userdata('id_user');
		$data_user = $this->ci->M_login->get($id_user)->row();
		return $data_user; 
	}
}

?>